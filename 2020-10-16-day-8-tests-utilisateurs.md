# Tests utilisateurs
*Nicolas Goyer*

## Tests utilisateurs

Avec l'émergence des méthodes agiles, le PO est venu remplacer l'utilisateur. Une personne valide et puis ça ne convient pas en production.

### Etapes d'un test utilisateur

*Ce que je conçois comme étape*

1. Déterminer le flow qu'on souhaite tester et les questions qu'on souhaite résoudre
2. Déterminer le type de test
3. Créer une équipe de testeur
8. Créer un document pour recevoir les compte rendu
4. Sélectionner les utilisateurs (nombre, socio démo)
5. Préparer le test avec un collègue (simulation)
6. Corriger le test
7. Inviter les utilisateurs
   1. Venue...
   2. Gift?
   3. Timing
   4. Préparer avec 2 personne
      1. L'interviewer
      2. Celui qui prend les notes

**Etapes**

1. Hypothèses
   1. écrans
   2. flux
   3. Questions qu'on se pose
2. Quel sont les profils
   1. Profils cible
3. Préparer le test
   1. Répétition avec nos collègues


#### 1. Hypothèses

* Identifier les buts principaux des utilisateurs et les hypothèses que nous cherchons à valider

#### 2. Rédiger les scénarios

* Décrire l'objectif (pas la manière d'y arriver - pas les termes exacte mais des synonymes)
* Phrases courtes
* Donner des données dummy préparées à l'avance pour anonymiser + tracer + résoudre les problèmes lié à des données personnelles
  * Construire une ficher identité + password...
  * Demander les données au **client**
* Mini jeu de rôle - mini scénario
* Laisser le **client** relire mais ne pas le laisser écrire ou modifier

#### 3. Protocole de test

**Plan:**
* Profil
  * Permet la sélection de la bonne personne
* Introduction
* Mise en situation
  * Présentation du Profil dummy
* Scénarios
* Questions post-test
  * Souhaitez vous revenir?
  * Questions ouvertes:
    * Quels sont les 3 choses que vous modifieriez?
    * Avez-vous quelque chose à rajouter?

#### 4. Pré-test

* Vérifier la pertinence
* Vérifier le temps nécessaire
* Lever les ambiguïtés
* Préparer les informations nécessaire
* Vérifier avec un collègue en dehors projet et le plus proche possible des utilisateurs cible

#### 5. Utilisateurs test

##### Combien

5 utilisateurs c'est bien **Nielsen** (80% couverture)

3-4 utilisateurs par groupe cible

==> 2 groupes de 3-4 personne

ou

==> 3 groupes de 3-4 personne

##### Comment recruter

* RS
* Affichette
* Clubs (sport...)
* Ecoles, associations
* Dans la rue - Guerilla ou pas
* Agence de recrutement - Defi line - opinions.be

**Filtrer les candidats**:
* Socio démo
* Métier
* La culture / langue
* Région
* Déjà utilisateur?
* Intérêt pour la thématique? (utiliser les extrêmes)

**Gérer le désistement**:
* 35% ne viennent pas ==> Surbooké (évent. annuler si pas nécessaire)
* Prévoir 20% de backup
* Bien informer + prendre en charge les frais
* Envoyer un reminder le matin ou la veille
* Calendar

##### Local et si pas à distance

* Local 
* Distance
  * https://jitsi.org/
  * Google meet
  * Teams

##### Usability Plan dashboard

https://www.userfocus.co.uk/pdf/usabilitydashboard.pdf

### Mise en oeuvre

**Tester tôt, tester souvent**

1. Wireframe
2. Prototype
3. Version en ligne
4. Systèmes concurrents

#### Organisation

1. Briser la glace
2. Scénarios
   1. Expliquer + Est-ce que l'utilisateur à bien compris? Est-ce qu'il pense avec fini?
   2. Passage du test + ne pas interrompre + écouter + pas de jugement
3. Post test

**Toujours de la bienveillance**

#### Equipe

1. Secrétaire: prendre des notes
2. Facilitateur: 
3. Client (opt): un représentant du client

Briefer et communiquer clairement les rôles de chacun

#### Données à récolter

* Temps réel, temps ressenti
* ...

### Résumé

#### Management summary

Résumé pour les décideurs

* 1 slide de compréhension global avec un point d'attention sur le plus important
* 

#### Rapport complet



## Exercice

