# Rencontre avec BOZA

* Bénédicte D'huart
* Patrick Berckmans
* Sébastien Barbieri

## Directive A11Y accessibility Act

Trou juridique dans la transposition de 2 directives - qui doivent être corrigé

* Les applications et sites de la RTBF sont hors champs

La directive ne s'applique pas à la diffusion publique

2019 --> 2022

Légalement en Belgique la première obligation d'accessiblité il y a une dérogation spécifique dans la directive pour les radio diffuseur.

Accessibility Act - Public et Privé en 2025 d'office

Discussion avec Madame Anne-Laurence Lequeue -  décret de la FWB

## BOSA - Fédéral + Organisme de contrôle

* Coordinateur de toutes les entités fédérés
* Probalement l'etnic --> FWeb
  * Sandra Velarde de l'Etnic

## VRT 

* 5 ans d'avance - know how à prendre - 
* La VRT à pris le parti de se mettre au norme avec l'accessibilité numérique

## Plan d'approche sérieux

* BOSA à dispo
  * Déclaration d'accessibilité avec l'Etnic idem déclaration vie privée
    * Plan
  * Tester l'outil et avoir le retour
  * Déjà maintenant - fait partie de l'audit
* Obligation pour le citoyen 23 Septembre 2020 - Morale
  * Il n'y a qu'une autorité publique qui peut certifier c'est l'autorité publique (ETNIC)
* Représentant - Délégué: en général responsable de la communication - une personne en charge permettra de coordonner le tout.
  * Former les rédacteurs
  * Former les community manager
  * Former pour la partie Audio
  * Former pour la partie Vidéo

## Todo

* Powerpoint --> XHU + BOZA + JPPH + COPIL DIGITAL
* Point au CoPil Digital
* CAP 48
* Argument juridique -> par email (Patrick Berckmans)
* Meeting avec la VRT pour faire la roadmap + Marc Walraven
* Meeting avec BOZA + XHU

