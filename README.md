# UX-Design

[Sharepoint ULB](https://universitelibrebruxelles.sharepoint.com/sites/GRP_UXDesign-Session092020/)

*Cours de UX design à l'ULB - note de cours de Sébastien Barbieri*

|Module|Thématique|PDF|Prof|Date|
|-----|----------|------|----|----|
| Planifier | [Introduction, concepts, théories, ROI](2020-09-22-day-1-introduction-concept-theorie.md) | [DESUX-01](cours/Introduction,%20Concepts,%20Théorie%20et%20ROI%20V3.pdf) | A. Dehon | 22 Sept. |
| Planifier | [Design Thinking](2020-09-22-day-1-wallet_challenge-processus.md) | [](cours/) | M. Caseau | 22 Sept. |
| Planifier | [Gestion de projets, compétences UX, hypothèses](2020-09-25-day-2-gestion-projet.md) | | A. Dehon | 25 Sept. |
| Découvrir | [Interviews besoins et observations](2020-09-25-day-2-interviews.md) | | A. Dehon | 25 Sept. |
| Découvrir | [Enquête et benchmark](2020-09-29-day-3-enquete-et-benchmark.md) | | R. Lambrecht | 29 Sept. |
| Découvrir | [Analytics](2020-09-29-day-3-analytics.md) | | R. de Robiano | 29 Sept. |
| Définir | [Analyse: Modèle mental, Personas et Users Journeys](2020-10-02-day-4-analyse-modele-mental-personas-user-journeys.md) | | A. Dehon | 02 Oct. |
| Définir | [Cards Sorting](2020-10-02-day-4-cards-sorting.md) | | A. Dehon | 02 Oct. |
| Définir | [Principes de design: Preception, attention, émotion](2020-10-06-day-5-principe-de-design-perception-attention-emotion.md) | | E. Mudjeredian | 06 Oct. |
| Concevoir | [Wireframe Statiques](2020-10-06-day-5-wireframe-statiques.md) | | E. Mudjeredian | 06 Oct. |
| Concevoir | [Design Studio](2020-10-09-day-6-design-studio.md) | | A. Dehon | 09 Oct. |
| Concevoir | [Prototypage Interfactif](2020-10-09-day-6-prototypage-interactif.md) | | M. Dufranne | 09 Oct. |
| Concevoir | [Au delà du wimp](2020-10-13-day-7-wimp.md) | | B. Dumas | 13 Oct. |
| Concevoir | [Evaluation Experte](2020-10-13-day-7-evaluation-expertes.md) | | A. Dehon | 13 Oct. |
| Evaluer | [Tests Utilisateurs](2020-10-16-day-8-tests-utilisateurs.md) | | N. Goyer | 13 Oct. |
| Evaluer | [Echelle UX](2020-10-13-day-8-echelle-ux.md) | | G. Gronier | 13 Oct. |
| Concevoir | [UX Writing](2020-10-20-day-9-ux-writing.md) | | R. Lambrecht | 20 Oct.|
| Evaluer | [Test Utilisateur - Lab](2020-10-20-day-10-test-utilisateurs.md) | | Nicolas Debue | 27 Oct.|
| Evaluer | [Accessibilité - inclusive design](2020-10-20-day-11-accessibilite-inclusive-design.md) | | R. Lambrecht | 30 Oct.|
## Méthodologie

![Liste des Méthodes](images/methodes.png)

Choix de méthodes

|Method|Objectif|Quali/Quanti|Attitude/Behavior|Speed|Prerequisite knowledge|Normative/Field/Free
|-|-|-|-|-|-|-
|5 second test|Evaluative|Both|Attitudinal|3|2|Field
|A/B testing|Evaluative|Both|Behavioral|1|3|Field
|Accessibility check|Evaluative|Qualitative||3|1|Normative
|Affinity diagram|Generative|Qualitative|Attitudinal|1|1|Field
|Analytics|Evaluative|Quantitative|Behavioral|2|3|Field
|Card sorting|Hybrid|Both|Both|3|2|Field
|Clickstream analysis|Evaluative|Quantitative|Behavioral|3|1|Field
|Cognitive mapping|Generative|Qualitative|Attitudinal|1|1|Field
|Cognitive walkthrough|Evaluative|Qualitative|Behavioral|3|3|Normative
|Communities feedback & micro-feedback|Generative|Qualitative|Attitudinal|1|1|Field
|Competitor analysis (benchmark)|Hybrid|Qualitative|Attitudinal|3|1|Normative
|Design studio|Generative|Qualitative|Attitudinal|2|3|Field
|Diary study|Generative|Qualitative|Attitudinal|1|1|Field
|Emotion questionnaire|Evaluative|Qualitative|Attitudinal|3|2|Field
|Emotion Tracking|Evaluative|Qualitative|Behavioral|1|2|Field
|Exploratory survey|Generative|Qualitative|Attitudinal|2|1|Field
|Eye tracking|Evaluative|Quantitative|Behavioral|2|2|Field
|Focus Groups|Generative|Qualitative|Attitudinal|1|1|Field
|Guerilla testing|Evaluative|Qualitative|Both|3|2|Field
|Ideation workshop|Generative|Qualitative|Attitudinal|2|3|Normative
|Information architecture|Generative|Qualitative|Behavioral|2|3|Normative
|interview|Generative|Qualitative|Attitudinal|1|1|Field
|Literature review|Generative|Qualitative|Attitudinal|3|1|Normative
|Mental model|Generative|Qualitative|Attitudinal|2|1|Field
|Mood board|Generative|Qualitative|Attitudinal|3|3|Free
|Online survey|Generative|Both|Attitudinal|2|1|Field
|Prototype|Generative|||3|3|Free
|Remote user testing|Hybrid|Both|Both|3|2|Field
|Shadowing|Generative|Qualitative|Both|1|1|Field
|Tree testing|Evaluative|Both|Behavioral|3|2|Field
|User Experience Questionnaire|Evaluative|Qualitative|Attitudinal|3|2|Field
|User testing|Evaluative|Qualitative|Both|1|2|Field
|UX check|Evaluative|Qualitative||1|1|Normative
|Wireframe|Generative|||3|3|Free

Source: https://airtable.com/shrJbMhtqmuRvAGXj/tblyGTkTG53c83daf

### @todo fix table here under

|Méthode|Etape|Objectif|Tools|
|-|-|-|-|
|Card Sorting|Concevoir|Architecture de l'information|OptimalWorkshop|
|Design Studio|Concevoir|Protoyping + Wireframing| |
|Low fidelity|Concevoir|Prototypping + Wireframing|wireframe sketcher - Sketch - Invision - Figma|
|High fidelity|Concevoir|Prototypping + Wireframing|Sketch - Invision - Adobe XD - Figma|
|Tree testing|Concevoir|Architecture de l'information|OptimalWorkshop |



