# Prototype Interactif
*Michael Dufranne*

## Introduction 

UX/UI pas possible de faire les 2 de front avec le même rôle.

* UX c'est un full time
* UI c'est un full time

### Type de prototype

#### Zoning

##### Avantage

rapidité,
cadre,
glossaire commun, économique, pas besoin de skills

##### Désavantage

pas testable, pas précis

#### Wireframe Low fidelity

Important de travailler avec une représentation montrant bien que c'est low fidelity (effet crayonné...) pour gagner du temps dans l'explication lorsqu'on présente pas les version finale

##### Avantage

Complet si contenu réel, permet de faire la todo liste, logiciel super simple (wireframesketcher), permet de ne pas se concentrer sur les élémentes de design si Black & white, facile à changer, facile de proposer 7 designs différent sans utiliser le temsp du designer

##### Désavantage

Les designers et les commerciaux déteste ça, nécessite de contextualiser (pourquoi c'est pas la version finale) 


#### Wireframe High fidelity

##### Avantage

Proche du rendu final et plus facile pour l'intégration

##### Désavantage

Lent, Difficile à changer, les testeurs se braquent sur le visuel, besoin de contextualiser, matériel de testing lourd à mettre en place

## Prototypage interactif

On se focalise sur le flow avec du low fidelity. On valide des scénarii.

Nécessite d'office des outils permettant l'animation ou directement dans le code prototypé (à jeter immédiatement).

Permet de tester la compréhension de l'interface par les utilisateurs. Avec des retours plus précis.

### Dans quels cas?

Si on sait au moins faire un low-fidelity.

Lorsqu'on veut apprendre et tester des scénario, découvrir les cas limites.

Pour préparer le [Briefing Créatif]

Si projet d'envergure c'est indispensable.

Après chaque test: corriger ce qui a été identifié.

* Lorsqu'on ne comprends pas ce que l'utilisateur cherche, ce qu'il comprends. On récolte ainsi des informations liée également à la sémantique.


## Outils

* WireframeSketcher pour les design low fidelity
* Sketch
* Figma
