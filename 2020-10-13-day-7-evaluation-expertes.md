# Evaluation Expertes
*A Dehon*

Cours en ligne: https://sites.google.com/adiscover.be/gim

* **Méthode**: Rapide - pas trop cher
* **Méthode précédente**: Personna
* **Méthode suivante**: Tests utilisateurs
* **Méthode liées**: Test utilisateurs (avant et après)

Permet de faire des évaluations très précise et des grilles de recommandation factuelle simple basée sur une méthode scientifique.

## Homogénéité

* Lien tjrs de la même forme
  * souligné en bleu par exemple

impossible de confondre des éléments sémantiquement différent si on garde un style bien défini

## La gestion des erreurs

* protections des erreurs: éléments de guidage + exemples + détections au plus tôt
* qualité des messages d'erreur: message pertinent, facile à lire et à comprendre
* correction des erreurs: facile à corriger

## Loi de Hick-Hyman

temps nécessaire à prendre une décision

![Loi de Hick Hyman](images/loi-de-hick-hyman.png)

## Actions Minimales

* Ne pas demander à l'utilisateur des informations qui peuvent être déduite par le système
* Menu simple sans imbrication
* Formualire avec données par défaut

### Cas de l'engagement

On peut souhaiter l'engagement avec plusieurs petites questions et des clics à chaque fois car le formulaire complet lui créerai un effet de refu

On peut contrebalancer avec la loi de hick et mesurer le temps nécessaire pour prendre une décision

## Grilles d'évaluation de Bastien et Scapin et autres

![Grilles d'évaluations non limitatif](images/liste-des-grilles-evaluations-methode-experte.png)
![Grilles d'évaluations non limitatif](images/liste-des-grilles-evaluations-methode-experte-2.png)

Les deux plus importante à maitriser: 
* [Bastien et Scrapin](#grilles-de-bastien-et-scapin) - [document complet de Bastien et Scapin (FR)](Outils/bastien-et-scrapin.pdf)
* Nielsen (US):

En fonction du cas de figure éventuellement travailler sur la persuation: Nemery et Brangier

Idéalement on travail avec une grille générale puis une grilel spécifique

## Méthode

1. Choisir une grille générale et une spécifique
2. Déterminer les experts (2-3...) Il suffit de 5 évaluateur pour avoir 75% des problèmes au delà le rapport coût bénéfice diminue
3. Prendre connaissance des personas
4. Périmètre bien spécifique (liste d'écrans précise)
5. Echelle de sévérité
6. Choisir un outils de centralxisation et de grille de lecture
   1. Powerpoint
   2. AirTable
7. Choisir un écran
   1. Parcourir toutes les heuristiques / tous les critères systématiquement
   2. Pour chaque critère déterminer une sévérité
   3. Confronter l'analyse avec les autres experts
   4. Prioritisation
8. Parcourir tous les écrans du scope

## Echelle de sévérité

![Echelle de sévérité](images/echelle-severite.png)


## Exemple de rapport

![Rapport type](images/exemple-rapport-evaluation-experte.png)

## Grilles

### Grilles de Bastien et Scapin

8 critères simples: [guidage](#guidage) , [charge de travail](#charge-de-travail), [contrôle](#contrôle), [adaptabilité](#adaptabilité), [gestion des erreurs](#gestion-des-erreurs), [cohérence homogénéité](#cohérence-homogénéité), [code dénominations](#code-dénominations), [compatibilité](#compatibilité) 

#### Guidage

Assiter l'utilsiateur dans l'emploi de l'interface, du système, ce qui l'informe pour le conduire dans ses interactions

Quelques sous-critères

##### Incitation

Incitation: Tous les moyens mis en oeuvre pour amener l'utilisateur a faire des actions spécifique
1. On lui dit où il est
2. On lui montre ce qu'il peut faire = affordance 
   1. Affordance Physique: Savoir comment on peut utiliser le device
   2. Affordance Cognitive: ex slide iphone
   3. Affordance sensorielle: vibration ou son ou signal lumineux
   4. Affordance fonctionnelle: ex calendrier on comprend la fonction on sait ce que c'est

Exemple:
* Bouton en contraste pour le parcours classique - mais attention de bien différencier bouton du reste - montrer si il est enabled ou pas également est important
* Par convention les boutons les plus importants en bas à drotie
* Par convention le boutons principal avec contraste le bouton secondaire sans contraste
* Libellé des boutons clairs
* Format déjà présente sous forme d'exemple ou de type dans le champ avec libellé et mandatory/not mandatory clair
* Montrer le champ actif
* Dire où l'on est
  * Titre aux fenêtre
  * Element de menu sélectionné ou/et breadcrumb
* Anmiation pour capter l'attention et l'amener à interagir (onboarding, fly over ...)

###### Remarques générales

A la mode actuellement: **Flat design** - donne un design très épuré mais perte de guidage dans le flat design - [Etude NNGroup](https://www.nngroup.com/articles/flat-ui-less-attention-cause-uncertainty/)


**Les visages** vont naturellement proposer au gens de les suivres il est donc recommander de faire regarder les visages vers la zone de l'itneraction

**Burger Menu** **Bar de navigation**: 
  * Burger menu: n'incite pas à faire des action
  * Bar de navigation va plus inciter l'utilisateur à agir

##### Guidage - groupement

Régi par les Lois de Gestalt: [Similarité](#similarité), [Proximité physique](#proximité-physique), [Groupement](#groupement), [Sens commun](#loi-du-sens-commun), [Symétrie](#loi-de-symétrie), [Consistence](#loi-de-consistence)

Regroupé ce qui est semblable par la nature et le format

###### Similarité

Les éléments identique au niveau sémantique doivent se ressembler

Les liens doivent être mis en évidence toujours de la même manière

Tous les éléments de la même nature doivent avoir le même format

Différencie les liens visité et non visités, les tabs doivent se ressembler...

###### Proximité physique

Grouper par proximité des éléments qui sont communs.

Aligner, regrouper permet d'associer les éléments et respecte la loi de proximité ce qui permet d'aider le cerveau à faire du lien

###### Groupement

Groupement d'élément via fond, via cadre, influence la façon dont on persoit l'information.

Les éléments connecté permet d'identifier les groupes, la chronologie dans un groupe...

###### Loi du sens commun

Même trajectoire = même forme (exemple menu, breadcrumb)

###### Loi de symétrie

Loi plus importante que la proximité. La loi de symétrie sera très facile à appréhander pour notre perception.

On peut aussi créer des asymétrie, ça permet d'attirer l'attention sur un autre élémént

###### Loi de consistence

Création de ligne de continuité, zone de scroll horizontal, bloc identique aligné verticalement

##### Feedback immédiat

###### Visuel

Led qui clignotte, icone avec indicateurs

60% des utilisateurs quittent la vidéos si elle met un certain à se charger avant la première image.

Informer l'utilisateur que quelque chose se passe.

Le fil d'ariane ou breadcrumb indique à l'utilisateur où il se trouve (très utile si l'utilisateur vient de google ou facebook).

Effet over, changement d'état dès qu'on pose le touch ou souris au dessus d'un élément.

Auto-completion et validation instantanée

###### Sonore

Son, musique, sonnerie...

###### Haptique

Vibration lorsque le téléphone sone

##### Lisibilité

* Luminance
* Contraste
* Espacement
* Dimension
* Longueur

###### Texte

Un texte en minuscule se lit 14% plus vite qu'un texte en majuscule

Garder les majuscule pour les débuts de mots clés, acronyme, titres très très court ou tags.

Pas de majuscule dans les actions (boutons, liens...)

###### Contraste

Vérifier le contraste avec par exemple le [contraste checker de webaim](https://webaim.org/resources/contrastchecker/) ou [WCAG contrast Checker Chrome](https://chrome.google.com/webstore/detail/wcag-color-contrast-check/plnahcmalebffmaghcpcmpaciebdhgdf?hl=en) et [WCAG Contrast Checker Firefox](https://addons.mozilla.org/en-US/firefox/addon/wcag-contrast-checker/)

**Dark mode** parfait pour de longues consultations, longs textes...

**Light mode** plus éfficace pour les interactions de base (titres, éléments, liens)

La notion de personnalisation va créé un aspect d'acceptation plus fort de l'utilisateur

#### Charge de travail

Optimiser le nombre d'action de l'utilisateur.

Diminuer la charge perçue

Diminuer la charge gestuelle

Diminuer la charge mentale, la concetration nécessaire

Afin de diminuer les risques d'erreurs et favoriser l'action

![Charge de travail](images/charge-de-travail.png)

Il y a un optimum à atteindre pas trop de stress mais pas trop de somnolence.


![Système 1 et Système 2](images/charge-de-travail-systemes-1-2.png)

L'enjeu est d'offir une expérience fluide qui convient au **système 1** et de faciliter le travail du **système 2** pour des tâches plus complexes.

##### Brieveté et concision

###### Méthodes de simplification

**Chunk** permet de mémoriser facilement des suites car le regroupement par élément facilement exprimable et le traitement rapide de l'information

**Loi de hick** plus on a de choix moins on a de choix. Si 24 choix: 60% d'arret mais 3% d'achat - si 6 choix: 30% d'arret mais 30% de choix.

##### Actions minimales

**loi de Fitts**

Distance à parcourir, taille des éléments interactifs

**Automatiser tout ce qui peut-être**

Sauvegarde automatiques, spell checking automatique, 

##### Densité informationnelle

**équilibre** entre trop d'info ou minimalisme

**Techniques**: 
* Un **visuel**, montrer est mieux que d'expliquer. Le cerveau comprend mieux les images que le texte.
* Tests des 5 secondes pour demander ce que la personne à retenu
* Méthode Corse (Cacher - Organiser - Réduire - Standardiser - Eliminer) - [Rémi Guyot Méthode Corse BlendWebMix 2019](https://www.youtube.com/watch?v=4lfW48_oDxo&feature=emb_logo)

#### Contrôle

Donner le contrôle à l'utilisateur sur les éléments de son interface.

Est-ce qeu tout ce qui se passe est intentionnel

Est-ce que l'utilisateur peut annuler, quitter ...

##### Action explicite

Action et réaction doit être transparente.

Pas de téléchargement automatique

Pas d'accessoire rajouté lors d'un rachat

Pas de popup après 20 lignes de défilement, l'utilisateur en comprends pas la réaction du système

##### Contrôle utilisateur

Il doit pouvoir annuler, revenir en arrière, changer les choses

Eviter les voie sans issues (toujours pouvoir annuler)



#### Adaptabilité

##### Flexibilité

Personnaliser l'interface selon ses habitudes et ses statégies.

Exemple: ligth mode, dark mode, police, changer les valeurs par défaut, changer le flow

##### Expérience de l'utilisateur

Interactions passées avec tous les systèmes.
Besoin de raccourcis, d'interfaces expertes.

Mode expert, mode novice dans l'application.

#### Gestion des erreurs

* **Protéger** contre les erreurs: Message type Récupération de la corbeille pendant 30 jours - vous avez oublier l'attachement
* **Informer** En cas d'erreur, info sur ce qui s'est passé, pouvoir en savoir plus si nécessaire et moyen de résoudre clair
* **Correction** des erreurs: donner des suggestions, être précis, indiquer au plus tôt champs pas champs, expliquer clairement le problème

#### Cohérence Homogénéité

Il faut conserver les choix de conceptions de l'interface tout au long de son évolution.

Loi de Gestalt: notre cerveau recherche des forme cohérente

Loi de jakob: les gens passe plus de temps sur les autres sites que sur le notre ==> il faut que notre site ressemble aux autres.

On peut innover mais en se basant sur ce que l'utilisateur connait au moins comme point d'entrée.

Scanning en **F** surtout pour les signes de contenu.

On lit la première ligne puis on scan verticallement et ensuite on relit une ligne horizontale.

Lecture en **L** scan vertical et recherche d'un résultat (presque tout le temps sur mobile)

Lecture en **Z** logo, call to action, tag line, call to action

Modèle titre et sous-titres

Modèle liens

Modèle d'évitement (évite toujours les premiers mots)

##### Technique du flou

Permet de déterminer si on voit un modèle de lecture sur le site

##### Cohérence - Design system

Garder toujours les mêmes couleurs, les mêmes bouton, la même cohérence et surtout avec des outils externes.

Avec un design system (Google material) Moins de probabilité de confronter l'utilisateur à ce qu'il en connait pas.

#### Code dénominations

* Icones = Labels?
* Boutons = actions liées?
* Jargon?


On doit bien penser aux labels, aux wording pour qu'il soit compréhensible, explicite et avec du sens.

L'aspect culturel du persona importe beaucoup pour les dénominations choisies.

**Form follow Function** la forme donne l'affordance et on comprend donc à quoi ça sert gràce à la forme. Si l'affordance n'est pas naturelle, le label aide.

**Skeuomorphisme, Neumorphisme**: représentation physique de l'objet dans l'interface (si un objet réel existe) - le neumorphisme va plus loin en reprennant tous les éléments de l'interface. - Problème d'accessibilité avec le neumorphisme, à cause des contours flous.

Un label = une icone

Sauf si populaire (croix pour ferme, wifi), sens commun (vélo, voiture), objet standard (attention aux générations et culture).

Loi de fermeture de Gestalt: **utiliser le vide pour pousser le cerveau à compléter les formes**.

Garder les concepts de temps, de mesure ... en fonction de la culture du persona cible.

#### Compatibilité

* L'interface % contexte
* L'interface % persona + besoins
* Compatible avec la tache de l'utilisateur
* Exploitable

Prendre en compte les caratéristique du persona dans son contexte, environnement.

Exemple:
* Date au format JJ/MM/YYYY si persona en Belgique
* Persona dans une fonction dédiée ou dans un flux
* Etat psychologique à envisager dans les process pénibles (encoder un login, erreurs...)

![Evaluation Experte de Bastien et Scapin en Mind Map](images/mind-map-evaluation-experte-bastien-et-scapin.png)