# Interviews besoins et observations

*Alexandre Dehon*

50 %  des startups font faillite dans leur première année d'existence

* Ex: JuiceHero - pilotage à distance - sachet de jus pré-fait - eau - ça coute moins cher d'aller chez l'épicier et ça ne sert à rien

> Lean: Get out of the building - **Steve Blank**

Rentrez en empathie

## User Needs interviews

* Méthod - choose
* Interviewer - be prepared
* Discussion guide - 

## Préparez votre enquête

* Décrire ce qu'on sait
* Décrire ce que l'on ne sait pas

### Que souhaitez-vous apprendre

Quels hypothèses souhaitons nous clarifier?

Comment ça se passe actuellement?

Quels sont les solutions mises en place? Est-ce qu'il paie déjà? Si il paient c'est un problème important.

Est-ce que c'est la meilleur méthode pour valider mes hypothèses?

|Urgence| Connaissance des utilisateurs basse | Connaissance des user importante |
|-|-|-|
|Elevée| Tests utilisateur non modérée en guerilla (via e-mail, forms ...)| |
|Faible| Interviews et observation d'utilisateurs sélectionnés | |

Utiliser la bonne méthodologie en fonction de l'urgence et de la connaissance

|Que| Qu'est-ce qui se passe? (quanti) | Pourqoi cela se passe-t-il? (quali) |
|-|-|-|
|Font-ils? (comportement)| | Observation test utilisateurs comportemental |
|Disent-ils? (attitude)| | |

Pour découvrir:
* Microfeedback - se renseigner sur les forums pour préparer les interviews
* Etude de marché
* Module de feedback
* UX Curve: méthode qui permet de retracer l'expérience de l'utilisateur de manière visuelle. 
    * Quels ont été les étapes
    * Quels ont été les moments positifs et négatifs
* Méthode croisées des interviews
* Email guerilla interview
* Cognitive mapping: interview visuelle où les gens mettent par dessins l'ensemble des actions que l'utilisateur va mettre en place (Domain driven design???)

#### Focus Groupe !!!

*Groupe de personne réunie pour parler d'un sujet*

Problèmes:
* Influence
* Dynamique de groupe
* Compormis
* Contrôle de l'information
* Comme une thérapie psychologique c'est individuel
* Problème de concurrence

Les Focus groupe ne sont pas utile à ce stade-ci.

Dans d'autre stade, choisir une solution, ça peut de venir interressant

#### Qui interviewer?

* En fonction des facilités d'accès commencer par interviewer les plus facile d'accès
* En fonction de celui qui va donner le plus d'information

#### Combien de personnes ?

1 c'est mieux que rien

4-5 c'est parfait

On s'arrete dès qu'on apprend plus rien, dès que ça devient rébarbatif, redondant

On est sur du qualitatif ==> pas besoin de bcp de gens

5 interviews max puis éventuellement changer d'aspect et d'angle et max 5 interviews ensuite.

#### Timing et budget?

Question à se poser

* Incentive - pourquoi les gens viennent, qu'est-ce qui les motivent à venir. Dédommager du transport ou du temps par rapport à leur socio-démo pour ne pas influencer l'interview.
* Matos - salles-lieu, matos d'enregistrement...
* Recrutement - soi-même ou en délégation via des agences pour le B2C si B2B demander la listes des clients interne
* externe
* délai
* lieu

Si on le fait en ligne, idéalement en visio conf. Le langage corporel est important et l'enregistrement est plus facile. Ce n'est pas trop cher.

Une interview dure 45' - 90' en général et 20' en guerilla.
Quoi qu'il arrive c'est vers les 70% qu'on apprend le plus, lorsque la personne est bien immergée.

## Se préparer soi-même

* Logistique
* Communication
* Observation

### Logistique

Accueil, bouffe, boisson, cadre légal, document, protocole clair....

Idéalement l'interview à 2:
- Une personne qui pose des questions
- Une personne qui observe et prend note (ou un enregistrement en plus + speech to text genre "temy" )

Lieu cocoon pas dérangé sans distraction, sans le téléphone si possible.

La Prise de note doit être faite dans le cadre d'un guide de discussion ou d'un canevas.

Enregistrer pour pouvoir y retourner

### Biais cognitif

*[Stephanie Walter biais cognitifs set de carte UX](https://stephaniewalter.design/fr/blog/a-la-decouverte-des-biais-cognitifs-le-jeu-de-52-cartes/)*

* **effet de primauté** lorsqu'on se souvient mieux des extrèmes (début ou fin d'interview), il faut donc tenir compte de manière égale des éléments en cours d'interview
* **biais de confirmation** lorsqu'on essaie de valider nos hypothèses avec l'interviewé et l'interprétation qu'on en a: si l'hypothèse est c'est pénible d'attendre et que la personne dit j'ai attendu 1 mois on peut croire que ça signifie que c'est pénible car pour nous c'est pénible
* **Observation** biais d'observation on amène les gens vers une hypothèse et on pose des questions plus ou moins fermées pour atteindre la confirmation de ce que l'on croit. Animer plutôt que manipuler.
* **chercher la cohérence** on a tendance à chercher la cohérence, des fils rouges, des éléments de cause à effet alors que potentiellement il n'y a rien.

### Se préparer soi-même

* Bannir les question fermée (est-ce pénible) --> question ouverte (qu'est-ce que tu en a resenti)
    * utiliser des mots comme:
        * comment
        * en quoi est-ce
        * pourquoi
* Explorer toujours les problèmes et pas les solutions
    * raconte moi une historie par rapoort à ce problème là
* 5 x Why
* quels sont tes priorités entre différtens chose pénible par exemple et demander pourquoi
* Eviter les réponse téléphonée - question qui contiennent la réponse. Qu'est-ce que tu pense de facebook qui est intrusif? En quoi Est-ce que tu as été embêté par la pluie ce matin?
* Si vous reformuler ça doit être sans jugement
* Laisser les silences, la personne rassemble ses idées, se restructure
* Accepter d'être ignorant du sujet
* Lorsqu'on reformule, éventuellement préciser un contexte mais rester très ouvert.

* Observer: les gestes, les mouvements, les actions, le non verbal
* Observer au travers d'une grille d'observation pour systématiser l'bservation.

* Préparer le UNIC canevas pour y prendre les notes
* Coder les notes car brut c'est très compliqué à utiliser
* Identifier les nouvelles hypothèses

* Interview type sur youtube: www.go2itech.org Demo interview 1

## Guide de discussion

* Exercice prévu:
    * Préparer un guide de discussion
    * Prendre note dans un UNIC canevas

Un guide de discussion permet de faire un interview en mode semi directif.

Un document qui est construit à plusieurs et qu'on valide avec le client.

Facilite la vie pour l'analyse

### Type de question

* **comportement** action autour de
* **opinion** pense de
* **connaissance** sais de
* **démographiqe**

**Astuces**:
* ~~si~~ --> quand

### Introduction

* se présenter, donner le cadre, signature du document pour l'enregistrement qui sera détruit, rappeler l'insentive
* mettre à l'aise
* choses générales très ouvertes pour mieux cerner la personne et la mettre à l'aise
* il n'y a pas de bonne ou de mauvaise réponse - vous ne travailer par pour la boite - la personne ne doit pas essayé de vous faire plaisir afin d'aviter le biai
* ne pas expliquer trop en détail

### Les problèmes

* exploration libre - situer la thématique
* que pensez-vous, raconter moi comment ça se passe
* quels sont les principaux problèmes... lequel est le plus important? pourquoi?
* explorer des problèmes en creusant de manière ouverte en demandant de détailler la journey pour explorer le domaine si nécessaire sur les hypothèses à tester qui ne sont pas mentionnée par la personne

* Travailler avec les UX cards: par rapport à la problématique, quels sont pour toi les besoins les plus importants. Top 3. Et demander pourquoi? Comment ça c'est passé. En quoi ça t'a apporté ça ou ça? C'est quoi un ... ? Qu'est-ce que tu entends par ...? Pourquoi? Pourquoi?

### Les solutions

Le risque c'est qu'on s'éloigne du besoin et que les gens aient un rejet. Si on va trop vite, on va avoir une shopping list, complètement déconnectée des besoins.

1. Quel solution elle utilise aujourd'hui
2. En quoi est-elle satisfaite? Pain points?
3. Autres manières envisagées?
4. Rattacher les solutions envisagées à un problème.
5. Si on parle des concurrents - demander ce qui l'attire chez les concurrents.
6. Est-ce que vous chercher des solutions?
7. A quoi ressemble voter solution?

Exercice d'exploration de solution: 
Si on vient avec des idées de solutions, les lister et demander à la personne de les lire, de les interpreter, les classer et les rattacher à un problème / besoin. Le but est d'analyser non pas la solution mais les problèmes liés.

### Conclusion

1. Souhaitez-vous rajouter quelque chose? -- silence
2. Interreser de participer à la suite du projet?
3. Indiquer pour nous des critères de qualités
    * verbal
    * non verbal
    * durée
    * nouvelle hypothèses
4. Directement après l'interview classer les notes dans un canevas

## Exercice - Préparer un guide de discussion

Concevoir un guide de discussion sous forme de 3-A4 par exemple

[voir le guide de discussion](2020-09-25-day-2-guide-discussion.md)

# Résultats

![Fig. 9](images/Screenshot_2020-09-25 ULB_Interview_groupe_7_montre_partie_2.png)

