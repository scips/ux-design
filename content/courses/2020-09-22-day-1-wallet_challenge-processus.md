# Wallet Challenge, processus

*Manuéline Caseau - architecte d'intérieur*

Cours pratique - pas de slide

Dixit - choisir une carte --> "c'est quoi pour vous le Design Thinking" en mode pop-corn aléatoirement

## Divergence 1 - Empathie 

En UX on ne fait pas d'hypothèse que l'on n'a pas vérifier sur le terrain. On ne reste pas assis sur son bureau.

On n'est pas dans le jugement, tout est possible, on va s'autoriser à écouter et à comprendre les besoins des utilisateurs. Sans a prioris.

## Convergence 1 --> Définition

Création des personas, création des parcours utilisateurs.

## Divergence 2 --> Idéation



## Convergence 2 --> Prototypage et test


## Experience du Wallet Project - Stanford Design Thinking par la pratique

* Imaginons le portefeuille idéal (thémaitque originale) --> out-dated ==> vrai problème du quotidien à imaginer qu'on peut résoude à travers une appli mobile.

Sur le post-it: écrire la problématique - derrière faire 4 cercles de même taille dans chaque quadrant.

Se lever et échanger le post-it avec quelqu'un --> mélange

Entre 2 avec chaqun un post-it décider comment répartir 7 points. (max 7 min 0) sur chaque post-it

Répeter autant de fois qu'il y a de cercles (4)

Total: max 21 

Appeler les post-it par point 21, 20, 19, 18, 17 ...

==> Idée: "ne pas savoir mon impact sur la planète"

## Aujourd'hui j'ai appris ...

* Que mon hypothèse de départ pouvait être complètement à côté et qu'il faut peut-être repartir d'une nouvell hypothèse: en effet il faut parfois revenir vers le top management pour leur dire que le terrain à des besoins différent
* A rester centrer sur l'utilisateur
* A être à l'écoute
* qu'en prenant le temps en amont j'en gagnerais après
* Que du développement non fait coûte moins cher
* A être au service et à me mettre en empathie
* A reformuler plusieurs fois pour m'assurer d'avoir bien compris le besoin
* A ne pas trop prendre la parole et a garder les questions ouverte, sans hésiter à laisser des blancs
* A changer de position pour faciliter par exemple si j'ai l'impression de ne plus être neutre
* A passer du Design thinking au design sprint 3-4 jours si nécessaire
