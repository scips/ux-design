# Analyse modèle mental, personas et Users Journey

*Alexandre Dehon*


## Mental models 

**Vice versa**: Emotion, souvenirs sous forme de ville intérieur.

[*Indi youngs - mental models*](https://interactiondesignresearch.files.wordpress.com/2012/03/mentalmodelssmall.pdf) [offline](Outils/mentalmodelssmall.pdf)

A travaillé pour VISA, IBM...

Les utilisateurs n'utilsent jamais correctement ce qui a été développé.

Peut-être qu'il y a un mismatch entre moi ingénieur ce que je crée et l'utilsiation rélelle des utilisateur.

Comment combler cet écart? Quel méthodologie je vais pouvoir utiliser?

Comment en terme de développement de produit peut on créer des étapes de développement

1. Définir un scope
2. Do the research
3. Résumé trsè ordonné
4. Qu'est-ce qu'on va en faire
5. représentation visuelle --> ville avec des tours

On a donc un modèle mental.

**Avantage**:
* extrèemement rigoureuse
* part de la base selon ce que les utilisateurs expriment vers un modèle high level
* Manière visuelle de montrer les choses

Possible d'y inclure d'autre insight: enquete quanti, feedback, interviews ...

Exemple: Contexte aller vour un fimm

Il existe d'autres manière de représenter: exemple - mind map

### Méthodologie

RRR: Rédiger - Regrouper - Recouper

#### Rédiger

**matériel brut**

* Interviews
* polls
* benchmark
* analytics

**task**: plus petite unité qui représente un comportement, une émotion de l'utilisateur 

Une **task** peut-être une croyance ou une connaissance (ex: je sais régler l'heure de ma montre, je connais l'heure de ma montre, je suis frustré parce qu'elle est en panne...)

5 types de **tasks**: 
* connaissance/croyance
* sentiments/émotions
* but, motivations
* action
* contexte

Exemple dans le cadre de la digitalisation des services d'entretient dans l'horlogerie

Pour rédiger: 1 **verbe à l'infitif** + 1 **complément**

+ Cohérence
+ empathie
+ spécifique

Par exemple prendre les interviews, les verbatims et transformer tout en tâches.

Verbes interdits: Gérer, Avoir ...

Idéalement les prendre en note directement durant l'interview

**Airtable** outil idéal pour faire ça --> export --> Outil d'indy young pour créer le diagramme automatiquement

**toujours bien noter qui à dit quoi**: exemple **Alexandre** --> **Craindre que ma montre soit griffée**

Ca permettra d'avoir des profils ensuite.

![Création du model mental](images/Screenshot_2020-10-02 ULB_Mental_Model.png)

**Travailler par affinité pour recouper**  

#### Recouper

Réduire l'information nous fait perdre une information. C'est acceptable car on concentre les atomic tasks --> macro tasks.

**Micro** --> **Macro**

Garder le lien entre macro tâche et micro tâche pour pouvoir refaire le fil d'analyse et répondre à la question: *"Mais ça vient d'où? Qui a dit ça?"*

**C'est important de pouvoir faire le lien dans l'autre sens**

Ne pas trop perdre d'information

#### Regrouper

Regrouper en TOURs par affinité

* Est-ce qu'on a deux tâche avec un verbe similaire
* Est-ce qu'on a un complément qui est analogue

Nos tours vont être un ensemble de tâche qui vont peut-être avoir une finalité commune.

Contrainte: **max** 7 tâches par tour

1. Regrouper
2. Nommer la tour

Dans un groupe on mixe des types différents de tâches (sentiment, émotions, actions, ...)

![Légende](images/Screenshot_2020-10-02 ULB_Mental_Model_legende.png)

Pas de post-it dans 2 tours

Comment grouper les tours?

**regrouper en moment**

Est-ce que la temporalité est la même, est-ce que la chronologie est similaire.

Suivre la chronologie permet de faire la user journey et de regrouper par moment différent.

Parfois il n'y a pas de chronologie mais ce sont des moments différents.

### Communication (équipe, management)

Communication d'une manière **mémorisable**, **transférable**, **actionnable** (roadmap, actions), **accessible** (toujours avoir accès)

N'evoyez pas votre modèle mental par e-mail.

Choisir le bon visuel! Conseils:

1. un modèle mental par personna
2. transformer le modèle mental en fiche persona
3. présenter sous forme de customer journey les étapes
4. Concevoir une empathy map
5. Reformuler le problème en racontant une histoire - Personna - action - barrier - ...

### Quelles solutions ça apporte

Solution proviennent du benchmark, des interviews, idéations

#### Priorisation

Choix des tours qui seront solutionnées (réduire le scope de la solution au MVP)

![Provenance des solutions](images/Screenshot_2020-10-02 ULB_Mental_Model_origine.png)

Priorité sur les tours.

#### Ideation

Travailler sur les questions:

* Comment puis-je suggérer une émotion positive pour le point .... ?
* Comment faire en sorte que mon utilisateur se sente plus efficace dans le cadre de ... ? (UX cards)
* (What if cards) Et si on amenait de la gamification ...? Et si l'app avait un comportement prédictif?


#### Sélection des idées

si c'est une tour dans le modèle mental, c'est **Désirable**

il faut que ce soit **viable** et il faut que **temporellement** se soit faisable.

**plusieurs tris possible**:

* Tri selon l'impact business + valeur pour l'utilisateur.
* Tri selon qui en bénéficie et quel est el type de persuasion.
    * Donner un conseil pour réparer la montre: qui  - utilisateur; type - explicite = Education
    * Funnel d'achat avec clauses non visible (responsabilité, livraison...): qui - business; type: caché = Dark Pattern
    * Mettre de la pub: = Quand il le faut (pas caché + bénéfice: business)
    * tracker la santé des gens pour eux: Nudge, pour le business: Dark pattern
* Déterminer les coûts de développement (S-M-L)
* Déterminer l'apport business ($-$$-$$$)
* Déterminer l'apport utilisateur ( :smile: - :smile: :smile: - :smile: :smile: :smile: )

Utiliser des méthodes d'animation de workshop [**liberating structure**](http://www.liberatingstructures.com/) un bon choix ici serait les **min spec**

**Min spec**

Exemple: *je veux que mon app donne les meilleurs conseil pour réparer les montres* - Virer tout ce qui n'est pas essentiel

## A retenir

* Méthode rigoureuse
* Méthode avec beaucoup de valeur
* Méthode analytique
* Méthode qui peut se faire en groupe --> va très vite + même méthodologie ==> facile à organiser et à rassembler
* met en avant les opportunité d'innovation très clairs

