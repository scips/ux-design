# Cards Sorting

*Alexandre Dehon*

Jakob Nielsen: 10% des problèmes sont lié à l'architecture de l'information.

en d'autres termes, "les gens n'arrivent pas à trouver ce qu'ils cherchent"


## Signaux de détection de la nécessité de l'amélioration

* Analytics
* Nommer les sections
* Contenu et type de contenu

Modèle mental permet de découvrir pour **Conceptualiser** --> **Card Sorting**

Comprendre comment l'utilisateur cherche et trouve l'information.

Accorder sur l'importance de certaine informations

Comment prioriser l'inforamtion

Méthodologie Ludique!

**limites**: 
- nécessite plusieurs itération pour arriver à un résultat stable
- si personna très varié --> personna divergent
- nécessit de trancher à un moment donné

**Méthode qualitantive** 

Nombre de personnes nécessaire: 15-30

* Quanti 15-30 personnes
* Quali car feedback précis des tuilisateurs

Besoin de combiner avec une enquète quanti

On peut la combiner, par exemple lors d'une interview

## Comment ça se passe en pratique

* Lister le contenu à tester
* Lister les éléments qu'on veux inclure dans notre navigation

Donner le contenu sous forme de carte

Demander aux gens de les classer 

Idéalement de manière individuelle

**Variante Delphi**: on demande a un utilisateur de classer le contenu puis on passe au suivant --> 5-6 utilisateur pour un résultat stable (converge vite mais pas forcément au bon endroit)

### Solution 1

Pas top car demande bcp de temps

#### Matériel

* Fichier excel
* Post-its

### Solution 2

Utilisation d'une solution informatique (on gagne du temps) même si on le fait en présentiel avec des post-its

#### Matériel
* Logiciel [optimal workshop](https://www.optimalworkshop.com/) + optimal sort
* Post-its - cartes préparées et vierges

#### Exercice

[Site web matériel de jardinage](https://app.optimalworkshop.com/a/34fztaje/optimalsort/surveys/532740/edit#/t/setup/cards)

#### Concevoir les cartes

* Que mettre sur les cartes?
  * Titre expicite à choisr avec soin
  * Petite description si le titre n'est pas clair
  * numéro de carte
  * éventuellement illustration

##### Titres

* Attention en utilisant toujours les mêmes verbes on influence l'utilisateur et donc on introduit un **biai**

Exemple: planter, semer, faire pousser, récolter, ceuillir

Il faut donc **utiliser** beaucoup de **synonymes**

##### nombre de cartes

Si les cartes sont fort **complexes** il faut réduire le npombre de cartes

L'échantillon influence le groupement ==> importance d'avoir un échantillon représentatif

##### utilisateurs / testeurs / sorteurs

Bien tester avec les différents personnas - déterminer lequels est le plus prioritaire en fonction des objectifs principaux de l'entreprise.

##### Choisir le niveau que l'on souhaite tester et la granularité

Eviter de demander de sélectionner des groupes et des sous-groupes

Il faut donc tester par niveau similaire en terme d'importance

##### Catégories

Prooser des catégories qui ont du sens et qui représente un nombre non négligeable d'éléments potentiel

Uniquement un niveau: Catégorie par contenu pas de sous niveau ou d'arbre ou de sous section

##### Recrutement

* Card sorting + enquete : 30 utilisateurs

##### Card sorting en groupe

* Ils doivent se mettre d'accord
* Ils doivent arriver à un compromis
* Ok si déjà fait avec d'autres utilisateurs en amont
* Idéal pour se mettre d'accord avec les départements marketing, ux, dev...

##### Déroulement

* Ne pas divulger l'objet de l'étude
* Dire qu'il n'y a pas de mauvaises réponses
* Demander si les personnes comprennent bien toutes les cartes (avant ou après si online)
* Mettez ce qui va ensemble et donnez un titre
* Classer par ordre d'importance
* Vous pouvez créer des éléments
* Si vous pensez que ce n'est pas possible de classer, vous pouvez les mettre de côté

##### Debriefing

##### Exercice

Créer 20 cartes de contenu et 4 catégories
Autoriser le rajout de catégories

##### Analyse

Lorsqu'on fait de l'analyse Delphi - l'analyse est déjà toute faite

Grace à des outils comme optimal worksho, ça facilite grandement l'analyse.

*Plusieurs facteurs*:
* temps de réalisation <1 minutes? >40 minutes
* abandon: nombre d'échec (pas terminé - pas envoyé) trop compliqué
* pourcentage par persona
* taux de réussite

**Grandes questions**
* est-ce que des contenus ont été classé ensemble?
  * si oui combien de fois - par combien de personnes?
  * est-ce que les catégories ont des noms différents?
* est-ce que du contenu à été non classé
* rang dans la catégorie

**Utilisateurs**: ne garder que les utilisateurs qui ont du sens (supprimer certains abandon, ou des personnas qui ne correspondent pas ou ne répondent pas à la mission)

**Cards**: analyser les top des classemens pour chaque carte, si la déviation est fabile, il y a un soucis probablement avec cette carte

**Catégories**: analyse des taux d'accord par catégorie enter les différentes personnes

**Standardization grid**: analyse croisée

Certains contenu sont classé de manière très très clair et on peut donc les prendre tel-quel.

**Similarité**: détermine les éléments qui doivent d'office être ensemble (>70%) et considérer la catégorie choisie

**dendogram**:

**best merge method**: algorithme qui permet de représenter la meilleure organisation de manière simple et compréhensible - fusionne les noms similaire

#### Utilisation des résultats

* Card sorting - ordering
* mindmap
* zoning

### Tree testing

Avec le card sorting on a encore des zones d'ombres

Avec le tree testing on crée une arborescence et on demande aux utilisateurs de trouver une information.

On demande aux utilisateurs de déposer dans l'arbre des éléments.

Se travail en itérations avec des questions précises pour des points d'éclaircie.

## Astuces

1. Structurer le contenu sans poly-hierarchies: pas d'éléments dans deux catégories différentes
2. maximum 7 éléments dans un menu sauf si ecommerce (amazon à bcp que 7 éléments) ou si les libellés sont clairs et exclusif pas de problème - mais en général < 7 + Le premier élément et le dernier sont les plus mémorisables
3. As-t-on besoin d'un moteur de recherche avec une navigation? On a besoin des deux. La navigation va avoir un impact positif sur les résultas de recherche car elle ajoute à la définition de l'élément en la catégorisant. Avec un moteur de recherche sur les sites 50% des recherches sont infructueuses. Il est donc important de faire une navigation
4. Pages d'accueil des catégories sont également des portes d'entrées qui permet à l'utilisateur de se rediriger
5. Navigation dois toujours être visible et le breadcrumb (fil d'ariane) doit être présent - les desginers vont vous dire que c'est moche mais c'est indispensable
6. La règle des 3 clics n'a pas de validité scientifique, cliquer sur suivant et cliquer sur payer ou valider ne demande pas le même effort (exemple swipe en mobile ...). Il faut résonner en terme de côut de click plutôt qu'en terme de nombre de clicks
7. Progressive disclosure - onboarding? - on ne donne pas tout tout de suite mais on offre des informations de plus en plus précises au fur et à mesure

## Sources

* Il existe des Vidéos sur Optimal Workshop pour expliquer les résultats

