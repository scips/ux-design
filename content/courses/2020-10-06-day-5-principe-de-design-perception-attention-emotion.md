# Principes de design: Perception, Attention, Emotion
*Emilie Mudjeredian*


**Wireframes**: Visuels basse fidélités

## Introduction

### Inspiration - Discovery (10-15% du projet)

Phase **d'inspiration** où l'on **collecte toutes les informations**
* Benchmark
* Audit
* Brainstorming

Souvent omis des planning, on se retrouve souvent avec peu de temps avant les premiers workshop.

Il faut savoir se prévoir du temps pour bien préparer.

Le cerveau à besoin de phases de repos et de réflexion.

La première phase doit pouvoir s'étaler pour donner la place au temps de cerveau pour avoir le temps de la réflexion.

Parfois on pose des questions bête, on soulève les pierres, on creuse ce qui va poser problème.

Si on la reporte plus tard --> influence sur les coûts.

Risque d'avoir une sortie de budget.

Si on ne se les posent pas, ce seront les devs qui vont poser les questions et ce sera un cout après.

### Conceptualisation - (30-40% du projet)

Phase **d'incubation et d'élaboration** où les gens attende de l'action de la part du Designer - phase où l'on concrétise avec plusieurs idées

Moment pour penser à coté => faire de l'innovation, ça ne coute rien à ce moment là.

Dépend du nombre de user testing 
* Avant: interview... 
* Après: interview...

Ici la moitié du budget est dépensé et le client n'a rien ... pas évident à défendre

### Réalisation (40-50% du projet)

Mode de travail différent où l'on travaille à tête reposée de son côté.

Phase longue

**Mood Board** toutes les références qui vont influencer le style du site - permet aussi de faire un briefing - idéal si l'on n'est pas soi même UI designer

**Spécifications** rajoute de la description aux écrans et permet aux équipes de développement de comprendre et permet la documentation

* Discussion avec les équipes de devs

## Design intuitif

~~**Pyramide de Maslow**~~ pas top par contre

**Pyramide de A. Walter** lead designer chez **Mashing magazine**

Pyramide de A. WAlter - étage 3

**Book**: Le design émotionnel - 2012

Toute expérience peut être désignée et peut se retrouver dans la pyramide

1. Fonctionnel: afficher la page dans un temps correcte
2. Fiable: pas du scam, donne confiance d'emblée
3. Utilisable: comprendre sur quoi cliquer, où aller
4. Agréable: prendre du plaisir à consommer un site internet ... permet la fidélisation et l'utilisation du temps de cerveau + crée de l'évangélisation

### Principes fondamentaux

L'utilisateur doit toujours savoir **où il est** (code: bread crumb, titres), **ce qu'on attend de lui** (utilisable de base, j'y suis pour une raison, je sais ce que je doit accomplir), **prochaine étape** (les gens on besoin de se projeter à l'étape d'après, qu'est-ce que le bouton va provoquer - contexte - que va t'il se passer derrière)

**Guidance**: on cherche à orienter l'utilisateur - ces utilisateurs arrivent avec des patterns qu'on doit garder en tête.

Exemple de pattern: Shopping basket est en haut à droite, si on le met ailleurs, on va en dehors d'un pattern existant. Exemple save = icone de disquette, tjrs difficile à changer.

**distraction**, les gens ont un taux d'attention très très bas, on a un nombre de jeton de concentration disponible ==> il faut réduire l'effort nécessaire. Il faut capter les gens en moins de 6 secondes.

**effort cognitif léger**: disponible pour remplir des tâches fastidieuses si nécessaire (formulaire ...) permet d'augmenter l'engagement

- Est-ce que je peux demander plus d'une chose sur une même page? Non, on demande plus d'effort à l'utilisateur. On a 1 objectif par page! Ca n'existe pas les objectif ex-aequo - pas possible d'avoir 100% pour tout
- 
### Composition

**Fold**: ligne de démarcation en bas de l'écran - sépare ce qui est visible de ce qui n'est pas visible

En général lorsqu'on design, il faut rendre visuel le **fold**. 

Il faut voir ce qu'il y a au dessus du **fold** comme une promesse. 

Il faut répartir le contenu, on ne peut pas tout mettre au dessus du **fold**. Ca surcharge le discours.

Il faut comprendre que le contenu est masqué, on laisse dépasser certains titres, paragraphes, photos.

**80%** on l'attention au dessus du **fold** --> 20% quitte ou sont inattentifs. --> NNielsen

**76%** des pages sont scrollées - 22% --> footer - 50% avant que la page soit finie de charger. --> Chartbeat

**Loi de Hick** = paradoxe du choix. Le choix c'est bien mais trop de choix c'est paraissant. Restaurant avec 50 plats, 50 entrées ... ou restaurant avec 3-5 plats du jour. 

**Biais cognitifs** - par défaut on veut plus, on veut toujours plus mais en réalité lorsqu'on teste on se rend compte que les gens ne travail qu'avec un subset réduit systématiquement --> important de challenger dès le début ce qui permet de réduire les coût après. ==> ne pas trop proposer.

**pré-sélection** ce qui intéresse x% des cas majoritaire car ces personnes n'auront pas d'action à faire sur l'écran.

**Loi de la proximité - Gestalt** par défaut notre cerveau associe des éléments qui sont proche spatialement. 

**Loi de Fitts** effort cognitif dépend de la distance et de la taille de l'élément cible. La loi est importante lorsqu'on pense les écrans dans leur ensemble. Pas de bouton trop petit, réfléchir en terme de flow d'une page à l'autre.

![FIG. 10](images/fig-10-flow-loi-de-fitts.png)

La souris reste là où le doigt de l'utilisateur était.

**Pattern de lecture** sont culturel (différent au japon et dans les pays occidentaux). En Europe - Amérique pattern en Z (blocs d'articles) ou en F (exemple recherche Google).

**F** on fournit un effort puis de moins en moins.

**Zones de repos** met en valeur les éléments - beaucoup de zone de respiration met en valeur le contenu

### Hiérarchie de l'information

* 70% des informations passent par la vue
* La UI est un pilier fondamental de la UX

### Mise en avant

* Taille
* Couleur
* Contraste

### Conventions

* emojis
* couleur (rouge - interdit, vert - positif ...)
* Pictogrammes - parfois nécessite une explication (exemple burger menu et avec explications pendant 5 ans) - on mémorise mieux les sons (même sous forme de voix intérieur lors de la lecture) les icones ne représentent pas de son ==> peut-être difficile - ne pas hésiter à utiliser les pictogrammes liés au jargon et à la culture maitrisé par la cible

### Ortho-typographie

S'associe à la notion d'orthographe. Exemple, espace avant le point d'exclamation.

* en fonction de la langue (anglais, français, néerlandais, allemand, ...) les règles sont à adapter à chaque langue

### texte

* **empâtement** au départ pour le dégorgement de l'encre, plus agréable sur l'impression papier.

* **sans-empâtement** demande moins d'effort à lire, plutôt privilégiées sur le digital. Sur les devices petits (apple watch, smartphone)

Ca empêche pas de varier de l'un vers l'autre pour attirer l'attention, identifier la marque, ajouter de la personnalité.

### Interactions

**Empan mnésique** c'est la capacité que notre cerveau à a mémoriser des éléments 7 -+ 2.

Je suis capable de faire un choix avec 7 éléments de tête, au delà je vais avoir difficile. Exemple: "Burger Quizz"

Je ne peux pas afficher de liste de plus de 10 éléments? Vrai dans une liste pour qu'elle soit visible et comparable en une seule fois.

Mon titre ne peux pas faire plus de 10 mots? Faux, ça à avoir avec la mémoire, personne ne doit mémoriser un titre complet. Ca n'a pas de sens.

![Fig. 11](images/fig-11-affordance.png)

**Affordance** interprétation qu'a notre cerveau d'un objet et qui permet de comprendre comment je dois l'utiliser et lorsque je l'utilise j'avais raison.

Le **flat design** est complètement anti affordance, le **Skeuomorphisme** était mieux pour l'affordance. **Le flat design** est une tendance, un mode.

Le **CTA** (Call To Action) n'est pas un bouton, ça peut être n'importe quoi, ça peut être le message (marketing) qui va généré une action de l'utilisateur.

C'est vraiment lié aux tendances (verbes à l'infinitif, couleur différentes, ...)

#### Check liste CTA

- [ ] Le style rend mon CTA affordant
- [ ] Mon choix de couleur est suffisamment contrasté
- [ ] J'utilise un verbe d'action
- [ ] Je suis le plus spécifique possible
- [ ] Je crée l'urgence *(optionnel mais ça marche bien - offre plus que 24h, plus que 2 éléments)*
- [ ] Mon CTA est visible *taille et positionnement son cohérents*
- [ ] La hiérarchie entre mes CTA est juste (idéalement pas nécessaire vu qu'il n'y a qu'une action par page)

## Design persuasif

Si on pousse trop on risque d'avoir un contre-effet.

Notre but est de **convertir**, **transformer**

Pyramide de A. WAlter - étage 4 - agréable

### Mental patterns

Nous ne sommes pas tous des flocons de neige identiques.

Il y a une distorsion au niveau du traitement de l'information càd **biais cognitifs** il y en a environ 100 de biais cognitifs.

Voici une sélection lié principalement au digital.

**Ancrage mental** le test des 5 secondes, que reste t il au bout de 5 secondes. On souhaite que ce qui reste soit positif. On ne va pas à un entretien d'embauche en pyjama, la première impression est la bonne.

**Biais de croyance** magasin *Lush* qui fait des savon de couleur avec des paillettes fluo, j'ai du mal à croire ce c'est naturel et bio.

**Biais de négativité** l'être humain va accorder plus d'importance à un message négatif. Attention danger par exemple

**Biais de statu quo** les gens n'aime pas le changement, chaque changement est compliqué, les gens veulent du nouveau sans changement, ou bien ils ont besoin d'un petit peu de temps pour accepter le changement. En interview on peut poser les questions deux fois: une fois au début et une fois à la fin, on aura une réponse différente car les personnes après discussion seront plus sensible au changement.

**Effet de leurre** souvent l'offre medium qui est l'offre de base, pour la placer on va créer une offre small et une offre large pour encadrer et créer un effet de leurre. On sait néanmoins que certaines personne vont prendre l'offre small et une partie de la population veut montrer sa richesse en prenant l'offre supérieure.

**Effet de primauté** première et dernière position sont des positions clés. Dans des listes de plus de 3 éléments par exemples.

**Effet de position centrale** semble contradictoire avec l'idée d'avant mais dans un autre contexte exemple offres small - medium - large dans une petite liste à plus de valeur.

**Effet de simple exposition** plus je répète, plus les gens considère que c'est vrai.

**Effet de disponibilité** empêche d'envisager une autre option, il ne reste que dans une sélection - motiver et ne pas forcer ici on offre plusieurs choix validé. Ca se fait beaucoup pour nous empêcher de challenger les propositions, on donne le choix à un nombre limité de cas maitrisé.

**Effet de choix par défaut** le monde l'internet reste un petit peu le farwest, le choix par défaut dans la politique du moindre effort permet de faire des choix sans que la personne ne s'en rende compte (newsletter, ...). Attention on a légiféré depuis.

**Effet Zeignarnik** tout ce qui n'est pas finalisé, reste en mémoire. Le cerveau n'aime pas lorsque les tâche ne sont pas finie. Les gens se sentirait mieux en finalisant leur shopping basket. On leur rappel simplement ce qu'ils n'ont pas fini... les gens vont avoir envie de finir ça.

### Réassurance

1. **Crédibilité** avec des témoignage client très clair
2. **Transparence** pas d'achat caché ou de frais caché disponible uniquement à la fin - pas de peur d'avoir de la comparaison, ça monter qu'on est à l'aise
3. **Accompagnement** fournir un call center avec une solution de type numéro de téléphone, chat ... faq 
4. **Engagements** livraison gratuite, engagement écologiques...

### Sollicitation

**très intrusif** mais ça fonctionne bien - attention aux non-mal voyants

### Adhésion

Comment faire en sorte que les gens **adhèrent** à notre message.

**Baby face power** comme dans Mailchimp avec leur mascotte *Freddie Von Chimpenheimer*.

### Gamification

On a besoin d'un **But** pour orienter les décision.

On a besoin de **règles** et d'une **acvancée** visible.

Qu'est-ce qui distingue d'un excel qu'on rempli toute la journée d'un jeu de click ou de farming? c'est la **participation volontaire**.

Pour rendre des tâche répétitive ludique, la **gamification** est une bonne solution. cf. **book** @TODO référence book

Lorsqu'on joue donne une satisfaction interne [*autotélique*](https://www.linternaute.fr/dictionnaire/fr/definition/autotelique/)

Une bonne application est l'équivalent d'une drogue

Modèle de **Swarm** pour stimuler les gens à donner leur données personnelles, ils ont créé un système de badge. Avec des badges cachés exemple: hangover.

**Récompense aléatoire** comme le bandit manchot *pinterest*, *instagram* récompense le scroll de manière très rapide et pour un effort très faible.

## Cas pratique

Travail sur un gabarit (canevas) 

1. Contruire la journey: choix des étapes, des écrans ...
2. Loi de Hick - un objectif par page + Call to Action loi de Fitts
3. Stratégie de design
   1. identifier les biais mentaux
   2. réassurance
   3. gamification

### User Journey

#### Poser le contexte
* **Journey Name**
* **Stage Name**
* **Stage Number**
* **User goal**

#### Décrire la base permettant de créer la user journey
* **Activities**: toutes les actions et évènements
* **Touch points**: tous les moments de contacts avec la marque
* **Emotions**: permet de déterminer si on capitalise sur le positif ou si l'on travail pour diminuer le négatif
* **Pain points**: tout ce qui m'empêche d'avoir une expérience agréable

#### Parti pris, positionnement
* **Opportunities**: opportunités pour l'entreprise ou pour l'utilisateur
* **Operable cognitive biases**: Image mentale créée dans les 5 première secondes, quels sont les effet de leurres, quels effet de rareté, quels effet de disponibilité (aspect secret ...) ...
* **Reinsurance**: 
* **Gamification**: effet de disponibilité, chercher après l'info confidentielle, cartes de fidélité ...

Travail sur l'horlogerie: par exemple

- Choisir une réparation
- Trouver un réparateur qui vient à domicile

#### Exercice



### Sketching

