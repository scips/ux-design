# UX-Design

[Sharepoint ULB](https://universitelibrebruxelles.sharepoint.com/sites/GRP_UXDesign-Session092020/)

*Cours de UX design à l'ULB - note de cours de Sébastien Barbieri*

|Module|Thématique|Résumé|Prof|Date|
|-----|----------|------|----|----|
| Planifier | [Introduction, concepts, théories, ROI](2020-09-22-day-1-introduction-concept-theorie.md) | | A. Dehon | 22 Sept. |
| Planifier | [Design Thinking](2020-09-22-day-1-wallet_challenge-processus.md) | | M. Caseau | 22 Sept. |
| Planifier | [Gestion de projets, compétences UX, hypothèses](2020-09-25-day-2-gestion-projet.md) | | A. Dehon | 25 Sept. |
| Découvrir | [Interviews besoins et observations](2020-09-25-day-2-interviews.md) | | A. Dehon | 25 Sept. |
| Découvrir | [Enquête et benchmark](2020-09-29-day-3-enquete-et-benchmark.md) | | R. Lambrecht | 29 Sept. |
| Découvrir | [Analytics](2020-09-29-day-3-analytics.md) | | R. de Robiano | 29 Sept. |
| Définir | [Analyse: Modèle mental, Personas et Users Journeys](2020-10-02-day-4-analyse-modele-mental-personas-user-journeys.md) | | A. Dehon | 02 Oct. |
| Définir | [Cards Sorting](2020-10-02-day-4-cards-sorting.md) | | A. Dehon | 02 Oct. |
| Définir | [Principes de design: Preception, attention, émotion](2020-10-06-day-5-principe-de-design-perception-attention-emotion.md) | | E. Mudjeredian | 06 Oct. |
| Concevoir | [Cards Sorting](2020-10-06-day-5-wireframe-statiques.md) | | E. Mudjeredian | 06 Oct.
