# Wireframe Statiques
*Emilie Mudjeredian*

## Introduction

**Pourquoi faire un prototype?**

On se projette, on essaie d'être exhaustif, on essaie de s'aligner.

On normalise la **communication**, le vocabulaire.

**Fail often, fail fast**

## Mise en perspective

### Type de prototypage

#### Zoning

pour faire une répartition spatiale principalement

**points positif**:
* très rapide
* l'utilisateur peu dessiné lui même
* force à la priorisation
* coute pas cher

**points négatifs**:
* fort sur l'imagination, très abstrait
* feedback pas vraiment réaliste

**Idéalement travailler avec le bon copy dès le zoning, éviter le lorem ipsum**

#### Wireframe

Normalement en noir et blanc mais on peut imaginer utiliser le vert, l'orange, le rouge et le bleu pour leur aspect informationnel

**biais négatif**:
* à cause du manque de couleur, de texte adéquat
* Ne pas faire de wireframe joli ça crée un biais d'acceptabilité

#### Content design et Wireframe haute fidélité

Idéal pour faire des tests

* très difficile de faire des modifications ==> forte charge de travail pour le designer

### Outils

* [Figma](https://www.figma.com/)
* Balsamique
* Adobe XD
* Invision
* Team sketch

## Aborder la réalisation

### Flow

Diagramme schématique qui considère en une seule vue l'ensemble des points d'entrée (email, rs, bannières). Schéma exhaustif où l'on met toutes les pages avec les liens entre-elles.

Le voyage ce fait dans un certain ordre chronologique.

Le niveau de granularité c'est le zoning.

C'est un outil de communication et de briefing.

### Touch points

Ce sont les moments où l'on peut faire passer des messages

* **Cross-channel**: internet des années 90 - où les canaux sont dissociés - ça ne se fait plus - vision fragmentée de l'expérience
* **mutli-channel**: internet des années 2020 - où l'expérience est différentes mais les données centralisée dans un CRM par exemple - si problème et appel au call-center il est automatiquement possible de récupérer les informations de plusieurs channel
* **omni-channel**: l'expérience d'une marque est l'expérience de la même marque et de la même expérience quelque soit le channel - tout connaitre de son client en temps réel - même expérience quelque soit le moyen de communication - on travail sur les écrans mais aussi sur les points de ventes sur tous les devices et dans tous les endroits (mail, application, site web, points de vente, tv, radio, publicité, présence physique)

### Adaptabilité

global web index stats in belgium https://www.moondustagency.com/knowledge-center/digital-landscape-social-media-belgium-2020

### Multi-device

### Spécifications

Artefact livrable en accord avec les équipes dès le début de projet.

**contient au moins**:
- wireframes
- les variantes
- ...

a définir au début

### Outils

* **Wireframe sketcher**

## Exercice


