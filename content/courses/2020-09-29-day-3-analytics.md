# Analytics

*Raphaël de Robianio* - Ergonline.be

## UX Analytics

Généralement pas un sujet très apprécié par les UX.

## Historique de l'ergonomie - Information Architect - UX

* Conception d'écran --> prototypage digital - super ergonomique
* Principe de re-design (on jete tout et on recommence)
* Tests utilisateurs avec Brussels airlines
    * Tests sur la cible avec scénario bien précis
    * Observation des 8 personnes
    * Recommandation
    * Implémentation partielle = +30% de conversion
* Energy manager avec Electrabel
    * Méthodes agile - itérations fréquentes
    * Mise en place de tests utilisateurs sur une fonctionnalité clé encore en cours de développement --> personne ne veut de cette fonctionnalité
        * Usabilité est le but des tests
        * Utilité n'est pas mesurée dans les tests mais dans les statistique d'utilisation

## Mantra

*On ne travail qu'avec des clients qui accepte que les tests utilisateurs sont source de vérité, et pas ce qu'un gars pense dans sa tête ou parce qu'il est CEO ...*

## Mise en place des techniques d'analyse

* Utilisation des analytics pour comprendre le comportement des utilisateurs
* Utilisation des analytics pour mesurer le comportement et les nouvelles fonctionnalités

## Exercice intro

aller sur anne-sophie.be

Petit jeu de rôle. Je suis gérant de l'entreprise. Prenez 5 minutes pour proposer 3 changements fondamentaux pour améliorer l'UX sur le site.

* Enlever les Animations sur les photos (après scroll, lentes, inutile)
* Choix de taille incompréhensible (taille 2 --> ???) 
* Ajouter au panier navigue vers le panier --> ajouter reste dans la page
* Filtre change en live
* Menu de filtre qui se replie tout seul après selection
* Filtre à gauche ou a droite mais pas au dessus?

### Résultat

En 5 minutes ce qui se passe c'est exactement ce qui se passe en salle de réunion.

On part de notre expérience personnelle, on a fait des assomptions et des recommandations.

C'est exactement ce qu'il ne faut pas faire.

On ne sait pas où focaliser notre attention.

La plus part des gens débarque sur d'autres type de page et ce qu'on croit est faux.

Parfois même si l'ergonomie n'est pas bien, le produit fonctionne.

### Que faire?

* Funnel d'achat (Page produit et panier est catastrophique)

> If you can't measure it, you can't improve it - *Peter Drucker*

* Besoin de chiffres pour mesurer et comprendre

> without data, you are just another person with an opinion - *W. Edwards Demming*

Soit vous avez du charisme, de l'influence, de ... soit vous avez des données.

## Les chiffres le disent

*"Ce n'est pas moi qui le pense, les chiffres le disent"*

Les analytics sont très rapides à mettre en place et à analyser.

Besoin de mettre les trackers en place avant de pouvoir remettre des conclusions.

Si c'est bien configuré, il est possible en quelques clics d'avoir une réponse.

Faire un test utilisateurs se fait en plusieurs jours. Récupérer les données analytics peut se faire en 2 clics.

## Analyse des méthodes à utiliser en fonction de

| Aspect | Définition | Exemples|
|-|-|-|
|**Quantitatif**|proportion d'un comportement présent dans un groupe| Quel pourcentage de persionne utilise le filtre sur le site d'anne-sophie. Est-ce que ceux qui utilise le filtre ont une meiller conversion? |
|**Comportemental (Behavior)**|Ce que les gens font||
|**Attitudinal**|Le sens selon leur comportement||
|**Qualitatif**|Qui ont un impact||


![Fig. 10](images/hometime-home.png) 

### Exercice - est-ce que les utilisateurs ont des difficultés à utiliser les filtres sur les pages catégories?

* Tests utilisateurs

### Exercice - quels sont les filtres les plus importants sur un site e-commerce de robes?

* Besoin de comprendre l'attitude + quanti ==> tests de masse en ligne
* éventuellement Interview d'abord (5-10 personne pas suffisant)

Ne pas se baser sur les stats uniquement il y a peut-être un biais si certains filtres ne fonctionnent pas ==> analyse de plusieurs sites d'e-commerce

### When should you use Analytics in your projects as a UX Designer/researcher/analyst/copywriter/etc?

1. **Recherche** Mieux connaitre le comportement des utilisateurs - technologie, intérets, source, socio-démographie + **Sondage**
2. **Engagement** Identification des problèmes et utilisation clé du produit + **Testing**
3. **Validation et mesure** mesurer l'amélioration du système

On identifie les problèmes aux travers d'analytics et on en profite après pour se demander **pourquoi** ==> Testing avec utilisateurs + online survey.

Data tells you what's happening, qualitative methods tells...

## Metrics

* Qu'est-ce qu'un metric: permet d'évaluer de manière quantifiable et mesurable une approche pour nous permettre de prendre les bonnes décisions.

### UX-metrics

Metric UX sont partagé avec le marketing

* **Usability** - a quel point c'est facile d'utiliser la fonctionnalité (temps nécessaire pour ..., taux de succès de la tache, moyen d'arriver, taux d'erreur)
* **Engagement** - interaction avec le produit, avec les features, fort lié au temps passé sur la plateforme (reading time, catégories explorées, scroll dans la page, pages vues)
* **Conversion** - (résoudre d'abord la usability et l'engagement) spécifique pour un objectif

### Procédure google pour définir les metrics

cf Goal signal metrics process

1. Définir les objectifs
2. Trnsaformer les objectifs en signaux
3. Transformer les signaux en metrics

Exemple - App de messagerie: 

1. **Goals**: que l'app soit utilisée pour comminquer avec leurs amis et leur famille
2. **Signals**: plus ils passent de temps à l'utiliser au mieux c'est
3. **Metrics**: Temps moyen passé par jour par utilisateur

#### Exercice Youtube

|Goals|Signals|Metrics|
|-|-|-|
|1. Users enjoy the videos they watch & then discover more videos|Click sur j'aime|% de like sur les vidéos
|1. Users enjoy the videos they watch & then discover more videos|Regarde les vidéos jusqu'au bout| % de vidéo vue jusqu'à la fin
|1. Users enjoy the videos they watch & then discover more videos|regarde plusieurs vidéos|Moyenne de vidéos vue par utilisateur / sessions
|1. Users enjoy the videos they watch & then discover more videos|Ils mettent un commentaires|% de commentaires par sessions
|1. Users enjoy the videos they watch & then discover more videos|Ils partagent|% de partage par session|
|2. Users quickly & easily find videos/channels that are relevant to them via the search|Ils click sur une des vidéos proposée par la recherche| % of CTR > 0
|2. Users quickly & easily find videos/channels that are relevant to them via the search|Il passe peut de temps à vérifier les résultats|...

#### Exercice Anne-Sophie.be

**Page catégorie**

|Goals|Signals|Metrics|
|-|-|-|
|Trouver le produit que je vais peut-être acheter|temps passé dans la page catégorie pas trop long avec clic sur un produit|
|Trouver le produit que je vais peut-être acheter|clique sur un produit de la page catégorie|
|Trouver le produit que je vais peut-être acheter|clique dans le menu puis clique sur un produit|
|Trouver le produit que je vais peut-être acheter|clique sur une autre catégorie sans avoir choisi de produit|

## GA Essentials

*Le plus utilisé et le plus accessible*

Exercice à faire par soi même

## Others

* Hotjar: mesure le comportements via funnel ou via vidéo comportementale + scroll + zone de click/touch + interactions/feedback

## AB testing

Une partie du volume test une version différente d'une interaction ou d'une page. On mesure ensuite pour déterminer ce qui est le plus performant.

Lorsqu'on a plusieurs idées de même impact potentiel, il est nécessaire de faire un AB test pour comparer et choisir la meilleur solution.


