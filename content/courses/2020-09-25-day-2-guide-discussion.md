# Guide de discussion

## Introduction

* [ ] Contexte: On s’intéresse au besoins dans le cadre du service de l’horlogerie
* [ ] Ça va durer environ entre 45 mn et une heure
* [ ] Toute la conversation va être enregistrée, filmée, est-ce OK pour vous?
* [ ] L’entretien restera confidentiel et anonyme
* [ ] Incentive: vous recevrez un défraiement sous forme de bon fnac
* [ ] Désirez-vous boire quelque chose?
* [ ] Est-ce que vous êtes confortablement assis.
* [ ] Nous allons vous poser quelques questions. Sachez qu’il n’y a pas de bonnes ou mauvaises réponses
* [ ] J’ai été engagé par une entreprise qui travail dans le domaine de l’horlogerie mais avec laquelle je n’ai aucun intérêt...
* [ ] Socio-demo:
    * Quel est votre profession?
    * Quel age avez-vous?
    * Est-ce que vous avez des enfants?
    * Avez vous des hobbies?


# Problèmes

* [ ] Situer la thématique: Je m’intéresse aux services lié à votre montre. Pourriez-vous me parler des problèmes que vous avez rencontré lors de la maintenance de votre montre.
    * [ ] Pourriez-vous me donner un top #3
    * [ ] Pourquoi le #1? En quoi ça impacte votre vie?
    * [ ] Combien de temps passez vous sur ce probleme? Pourquoi?
* [ ] Spécifique
    * [ ] Pourriez-vous me parler de la dernière fois que vous avez eu un problème avec votre montre?
    * [ ] En quoi était-ce un problème?
    * [ ] Qu’avez-vous fait?
    * [ ] Qu’est-ce que vous aimiez de la solution que vous avez trouvé?
    * [ ] Expliquez-moi du début à la fin comment vous avez vécu cette expérience.
    * [ ] #couts Quel est votre avis sur les prix pratiqués?
    * [ ] #distance, #horaire  Comment vous rendez-vous chez votre réparateur, pourquoi, quand?
    * [ ] #choix des pieces Trouvez-vous votre bonheur dans les choix qui vous sont disponibles chez votre horloger?
* [ ] UX cards
    * [ ] Selon vous qu’est-ce qui sont les besoins les plus importants par rapport à la problématique de la réparation de montre:
        * Contrôle/Sécurité
        * Influence/Popularité
        * Efficacité/Compétence
        * Autonomie/Indépendance
        * Relation/Appartenance
        * Bien-être
        * ...
    [ ] Pourquoi?

# Solutions

* [ ] Quels solutions avez-vous trouvé pour résoudre ce problème?
* [ ] Qu’est-ce qui est bien et moins bien dans cette solution?
* [ ] Comment envisagez-vous les solutions idealles? Quel problème sont résolu?
* [ ] Avez-vous déjà exploré d’autres pistes de solutions? Lesquelles? Pourquoi? Qu’en avez-vous pensé? Qu’est-ce qui vous attire?
* [ ] Avez-vous deja payé pour une solution?
* [ ] Est-ce que vous cherchez d’autres alternatives? Lesquelles?

# Conclusions - on the record

1. Souhaitez-vous rajouter quelque chose? -- silence
2. Intéressé de participer à la suite du projet?

# Conclusions - off the record

1. Indiquer pour nous des critères de qualités
    * verbal
    * non verbal
    * durée
    * nouvelle hypothèses
2. Directement après l'interview classer les notes dans un canevas
 
 

