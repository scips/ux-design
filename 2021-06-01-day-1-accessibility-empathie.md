# Cours

* Empathie
    * Tous
* Gestion
  * Chef de projet
  * UX lead
  * PO
* Design
  * Design d'interaction
  * Design Visual
  * UX
* Editorial
  * Copywriter
  * UX Writer
  * Architecte d'information
* Technique #1 et #2
  * Front-end HTML
  * Back-end


# Empathie

Cours donné par Régine Lambrecht

Présents: Alexia Delhaye - orange; Amélie Steenput - Delaware Consulting; Nicolas-Xavier Ladouce - Service au Public @ RTBF; Philippe Blanjean - Graphiste; 

Question:
- Est-ce que je dois tester avec un lecteur d'écran? OUI!!!! Il faut tester avec un lecteur d'écran.
- 

## Qu'est-ce que l'accessibilité

- c'est un défi pas une contrainte
- ça doit commencer avec la charte

> *Mettre le web et ses services à la disposition de tous les individus, quel que soit leur matériel ou logiciel, leur infrastructure réseau, leur langue maternelle, leur culture, leur localisation géographique, ou leurs aptitudes physiques ou mentales* - Tim Berners-Lee

**A11Y**

## Empathie

Endométriose, pas possible de rester longtemps assise ...

Déficiences permanente ou temporaire ou apparentées - pas de son dans une bibliotheque (pas de son) - pas de souris - bras cassé - ... - langue étrangère, tooltip on mouse over ...

Design Toolkit de microsoft, WAI perspective.

![Ratio](images/ratio.png)

### Senior

Tests avec les seniors: 1 an de surf + capable de faire des paiement en ligne
Attention au Cookies, très compliqué pour les seniors

Un seul moyen, tester avec eux.

![Voo Cookie](images/voo-cookie.png)
Exemple: site de VOO qui s'amuse avec les messages de Cookie à tester.

* Combien de personne souffre d'un handicap permanent: 15% - argument fort mais celui qu'on aime le moins parce que si il n'y a pas assez on perd au niveau ROI

> 15-20 % de personnes atteinte de handicap

Pas des stats de la WHO (Organisation mondiale de la santé) - uniquement du déclaré, il y a donc probablement plus

### Pourquoi de l'empathie?

1. Pitié
2. Sympathie
3. Empathie
4. Compassion

La pitié et la sympathie ne sont pas suffisant, ça fait un espèce de minimum viable.

Les outils de simulation permettent de créer l'empathie en aucun cas ils sont valide pour des tests, on préférera toujours les vrai outil des personnes handicapée pour une simulation.

### Aveugles

Travaille sur apple, sans règle braille (tout le monde ne connait pas le braille), principalement en voice. Ils connaissent le clavier normal

Tester le voice sur le français pour comprendre le degré de précision.

Par ordre de priorité d'abord maitriser AnySurfer, ensuite RGAA français (très précis).

Les aveugles swipe avec le lecteur d'écran pour passer de zone cliquable en zone lisible en zone lisible. ==> pas possible d'avoir un carrousel.

Toutes les alternatives sont lues pour les personnes aveugles, ils utilisent beaucoup la navigation kioske.

Bouton pour les paiements ... tout doit être accessible sans la souris

Comment ça se passe avec les pubs?
* les pubs sont lues. Mais il faut que tout soit bien structuré et avec des titres (H1-H6) à éviter dans les pubs.
* Ils travaillent avec une vue "table des matière et navigation par titre et par niveau de titre".
* J'ai jamais vu un aveugle qui lisait de haut en bas!

*[Lecteur pour aveugle](images/stat-lecteur-decran.png)

Test avec SilkTide

Skip link pour le coeur de page, très important d'avoir un bouton: aller au contenu principal pour les personnes avec une seule. Le skip link doit être visible!

Sur RTBF.be

"Aller au contenu principal" est utile pour les personnes avec un seul bouton pour naviguer mais moins pour les aveugles.

Du sport c'est aussi des infos.

Problème avec la croix (X) button? Pourquoi pas fermer?

Enlever Read Speaker - Charlatanisme

#### Test avec nvda

Activer Element List

Priorité:
* Les titres
* Les liens
* Les formulaires

Moins important:
* Landmark

##### Organisation des titres

Titre: "Menu principal" - pas bon pour le référencement mais **très bien** pour les personnes aveugles

Le niveau doit être bien organisé mais pas si important

##### Liens

Structure par lien importante

Important que rtbf.be --> Accueil RTBF.be
Important que contact --> Contactez la RTBF

Bien garder le A de Accueil, le C de contact

Les WCAG acceptent les notions: en savoir plus, mais ce n'est pas très utile

##### Formulaires

##### Landmark

HTML5 et peut utilisé par les personnes aveugles

N'est pas enseigné dans les cours de navigation pour personnes aveugles

Par contre une bonne idée de les maintenir et de les utiliser pour la structure

#### TalkBack

Premier Touch lit
Deux Touch sélectionne

les langues fr, nl, de, en... idéalement faire en sorte que ce soit français, nederlands, deutch, ...

les liens dans une liste simple

Swipe pour passer de mode:
* titre en titre
* lien par lien
* commande (formulaire)

Menu spécifique a talk back: lire toute la page

### Vue limitée

#### Magnifier

Activer le magnifier et se rendre compte de la navigation par zone

#### Daltonisme

* 8% des hommes
* 0,45% des femmes

Multiplier les source d'informations: couleur/image + texte

Silktype: outil d'empathie mais pas de test

ChromeLens (F12 toolbar de développeur)

Importance de souligner les liens pour les rendre visible des daltoniens

**Graphes**: Attacher les légendes directement au graphes

#### Autres troubles visuels

* Cataractes
* Vue en tunnel
  * Contenu interactif proche de la zone de click
  * être cohérent dans l'approche
    * langue en haut à droite
    * moteur de recherche en haut
    * menu activable qui ne se cache pas lorsqu'on quitte la zone
    * feedback important pour comprendre que quelque chose à changé dans la zone visible
* Vue en dehors de la zone de focus (cataracte ...)
  * toutes la zone d'input doit être clickable pas juste la checkbox
* Strabisme
  * décalage de charatère important

### Dyslexie

* Inversion de lettre
* Impossibilité de passer d'une ligne à l'autre
* Outil qui:
  * Augmentation de la distance entre les lignes et parfois changer la couleur d'une ligne à l'autre
  * Améliorer l'empatement

Pas besoin de choisir l'empatement, les personnes qui en ont vraiment besoin ont des addons qui s'occupe de ça

Il y a un test a faire avec une distance de ligne et ne distance de caractère bien précise pour wcag, pour le reste on fait ce qu'on veut.

Ecriture inclusive: point médiant - pas problématique pour les dyslexies. Mais idéalament à éviter en utilisant des formes neutres.

### Personne à mobilité réduite

Skip link doit être visible à la navigation kiosque

### Trouble de concentration

Utilser le reader mode et tester (firefox)

### Trouble auditif

Un adulte sourd de naissance à un vocabulaire de 12 ans

### Trouble moteurs

Funkify
Web disability simulator

Règle: Zone de click est un critère AAA mais il va passer en AA

### conclusions de l'empathie

*Essentielle pour certains
et
Utile pour tous*

8px de zone non clickable entre deux boutons

## 13 mythes

### Mon site est accessible parce qu'il a des plugins : FAUX

Le readspeaker est le seul vraiment pas bon

Pour l'agrandissement des caractères et du contraste, les gens n'attendent pas d'avoir ça, il l'ont au niveau des settings de l'os.

* Font ok pour ceux qui sont en transition (pas encore dans le mode OS)

Tout ce que vous faites avec un plugin peut être fait en CSS ou en JS.

Attention au !important qui ne pourrons pas être modifiées.

### Mon dev sait ce qu'il faut faire et mon site est certainement accessible

Non, les écoles ne forment pas et si elles forment, il faut êter à jour tout le temps.

### C'est juste un truc de développeur

Celui qui bosse le plus c'est le dev. Néanmoins, ça impact l'ensemble de la chaine.

Loin d'être une question uniquement de développement.

### On le fera à la fin

Erreur de débutant.

Si on le rajoute à la fin, on doit refaire 50% du code.

@todo image de devops.com avec l'ux et l'accessibilité

### On va tout automatiser: audit et correction

C'est complètement faux, aucun outil ne fait tout à la place

* Facility
* AccessiBe

c'est de l'arnaque et de la nuisance

### Les sites accesibles sont laids

* Pairdaiza
* BBC
  * BBC Design system

Vous pouvez utiliser des slider mais il faudra les coder d'une certaine manière

### L'accessibilité ne concerne qu'une partie de la population

* 15% qui est concerné
* mais tout le monde à un handicap à un moment ou l'autre de sa vie

### Ne corrigeons que pour les personnes aveugles

Faux on ne se concentre que sur un handicap mais ce n'est pas une bonne idée

Ce n'est pas éthique, ce n'est pas ce qui est dans la loi

On ne peut pas prioritiser

### On va faire une version texte en parallèle

* Très mauvaise image de marque
* Pas envie d'aller dans la version Ghetto
* Pas toutes les fonctionnalités

### Mon site est accessible il a reçu un label il y deux ans

* En Belgique: uniquement AnySurfer
* Attention à ne pas se reposer sur ça
* 2 ans cycle de révision

### Mon site est accessible, j'ai 100%

* une partie des utilisateurs n'iront jamais sur le web

Par contre et bénéfique pour google

### Mon site n'est pas accessible mais je ne risque aucune amende: clause de charge disproportionnelle

* On peut à condition d'avoir fait un audit avec estimation du coût qui montre la disproportion
* Intéret d'avoir un rapport d'audit qui nous aide à mettre des priorités

### Un site est accessible ou pas

* C'est très décourageant car on doit favoriser l'évolution progressive - thanks to anysurfer
* C'est parce qu'on connait les priorités qu'on peut faire une vrai feuille de route et c'est indispensable

## Conclusions

* Virtual Panel: how persons with disabilities use the web (deque)
* Access-key.org

* Meilleurs formation
  * Deque
  * Webaim
  * Duka

* accessibility maze pour devenir expert en tab, enter, space, fleches 

## Tools

* Design Toolkit de Microsoft
* tiny.cc/ulba11y
* https://www.w3.org/WAI/perspective-videos/
* https://www.anysurfer.be/fr
* https://www.numerique.gouv.fr/publications/rgaa-accessibilite/
* [NVDA](https://www.nvaccess.org/download/) - OK POUR TESTING
  * Focus Highlight addon
  * Activer le speech viewer
* Talk Back Android - Android ; - OK POUR TESTING
* [JAWS](https://www.freedomscientific.com/products/software/jaws/) - OK pour TESTING
* [Apple accessibility vision avec notamment VoiceOver](https://www.apple.com/accessibility/vision/)
* [Silktide](https://chrome.google.com/webstore/detail/silktide-website-accessib/okcpiimdfkpkjcbihbmhppldhiebhhaf) Simulation - apprentissage PAS DE TESTING
* [Apple Voiceover](https://www.apple.com/accessibility/vision/)
* [Team Viewer](https://www.teamviewer.com/en/products/teamviewer/) Pour tester et partager les résultats d'un test en utilisant du remote control sur android
* [ChromeLens] - Développeur toolbar de chrome
* [Daltonizer] - Pour devenir datltonien sur iOS/Android
* [SimVis Android] - Simulation problème visuels
* [Eye view Android] - Simulation problème visuels
* [Beeline Reader] - Dislexye
* [Web Disability Simulator] - 
* [Distractabilty simulator](webaim.org/simulation/distracctability)
* [Chrome Funkify]
* [Reader mode firefox]

## Todo

Microsoft design toolkit

