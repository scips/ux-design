# Gestion de projets, compétences UX, hypothèses

*Alexandre Dehon*

**Code Slido**: #A703

## Maturité UX

8 Niveau cf NNGroup 

|Niveau| Description|
|---|---|
|1| Anti UX|
|2| Si c'est bon pour nous ...|
|3| Un petit groupe qui s'intéresse à l'ergo, veut initié une démarche, effort sur certaines parties du projet|
|4| UX est important et budget consacré (Consultant + Formation) approche avec externalisation |
|5| Un département UX |
|6| Tous les projets de conception centré utilisateur|
|7| Tous les projets passe par la conception mais avec metrics en plus|
|8| Entreprise de A-Z comptable - marketing -... se base sur les méthodes de conceptions utilisateurs|

10 - 15 ans pour passer du niveau 1 à 8

Accélérateur mais piège... si une personne part, si le processus n'est pas bien compris dans l'entreprise ...

Pour passer de l'un à l'autre c'est important d'avoir un win case dans lequel on met en évidence la plus value du projet.

### Que faut-il faire pour augmenter notre niveau de maturité UX dans l'organisation?

* Article de McKinsey: Les E. qui consacre les étdues sur des études utilisateurs vont avoir de meilleurs résultats.

#### 4 piliers

##### Leadership analytics

L'UX est un département comme le commercial, comme le marketing, comme avec des KPIs

##### Talents transverse

L'UX peut-être piloté par une ou plusieurs personnes mais il est éssentiels que les autres départements y soient également sensibilisés (dev, marketing, directeurs)

##### Iteration continue

Il faut être dans une démarche agile ou l'on test et améliore en continu

##### L'utilisateur

Construire les décisions sur base du feedback utilisateurs de manière systématique

#### Design score

Revenu directement lié au Design Score

#### Orchestre Philarmonique

Le processus est la partition (exemple design thinking ...)

Les musiciens sont toutes les personnes impliquées pour jouer la partition

Il faut ensutie commencer à jouer

#### Faire un plan d'action

1. Exemple avec le Design Thinking - 1990 IDEO

    * Postulat de départ - ex. Construire une chaise avec 3 pieds est revolutionaire, les étudiants de l'ulb ont du mal a comprendre les danger lié au Covid.
    * Patricia Moore --> basé sur l'**empathie** - ex interviews, ghosting ...
    * **définition** on spécifie le problème (ex. règle COVID dans les auditoires)
    * **idéation** on génère des idées
    * **prototypage** on va faire une maquette
    * **tests** on test la maquette, récolte de feedback

    ITERATION x 2, 3, 4 ...

    * UX Research: **empathie**, **définition**, **idéation**
    * UX Design: **prototypage**, **tests**

    **Important d'aller jusqu'au bout, ne pas s'arreter à la phase conseils == waste**

2. ISO 9241

    Activités et livrables

    |Activités|Livrables|
    |-|-|
    |Contexte|Spec. Contexte|
    |Mesures, critères et contexte|spécification|
    |Evaluation|Déclaratipon de conformité|
    |Révision de la conception|Produit amélioré|

    Ce mode de fonctionnement passe très bien dans les entreprise qui concoivent pour des ingénieurs ou des spécialiste

    Beaucoup de documents et de procédure normal avec les normes ISO

3. Waterfall - Agile

    * **Waterfall**
        * écriture du cahier de charges complet
        * passage aux équipes de dev
        * Livraison

        Bcp de temps passe sans retour utilisateurs et donc bcp de déception à la fin
    
    * **Agile**
        * On développe les fonctionnalités petit à petit
        * On test régulièrement avec le client
        * On release et on analyse les résultats
        * On passe à l'itération suivante

        Scrum est une méthodologie agile qui itère sur des laps de temps court 2-4 semaines.

        UX vient en amont de la méthodologie SCRUM

        * Inteview --> Product Backlog
        * Guidelines--> DoR, DoD
        * Prototypage
        * UX Review des prototypes
        * UX test pour récupérer les tests utilisateurs

4. Lean

    * **Lean**
        * **6 Sigma lean**: Gaspiller le moins possible
        * **Lean Startup**: On commence petit, on expérimente sur des faits, on itère - Empathy, Créativité, Rationalité
            * Books:
                * **Lean UX** : Lean startup + design thinking
                * **Running Lean**: interview

        UX/Business validation --> important de valider le business en plus du produit en permanence.

        * P (Plan) - Hypothèse
        * D (Do) - MVP
        * C (Check) - experiment
        * A (Act) - Research --> nouvelle hypothèses

        Moment de PIVOT pour savoir à chaque fois si l'on continue

## Processus UX

1. Découvrir - comprendre le projet, les personnes, enquetes, inteviews ...
2. Conceptualiser - MVP, fonctionnalités, flux ...
3. Designer - Concevoir l'identité et interface/le prototype
4. Tester - Test avec les utilsateure + feedback et retour 3 voir 2
5. Développement - le produit devient réel
6. Ecoute --> améliorer --> découvrir

A chaque étape du processus on a une ou plusieurs méthodologies qui sont appropriées

Le processus peut prendre un an mais aussi 5 jours.

Exemple: Facebook Design Sprint en 1 semaine

|Lundi|Mardi|Mercredi|Jeudi|Vendredi|
|-|-|-|-|-|
|Mini enquete|ideation|converger|protoypage|test|

Bien planifié et bien organisé ça peut tenir dans une semaine

Il n'y a pas qu'une seule manière, il y en a plusieurs qu'il faut adapter à notre organisation

### 5' de réflexion sur comment implémenter dans l'E un des processus

> Mettre en place les systèmes de mesure des résultats pour être data driven et plus basé uniquement sur l'intuition, ça permettra de convaincre le C-level par les chiffres et non plus par des "verbatim".

* Pour mettre en place un tel process il est nécessaire de systématisé la présence et le rôle des UX dans les équipes SCRUM et de leur fixer des objectifs clairs en terme de livrables attendu.

* Par exemple: je souhaite avoir un power point avec les fonctionnalités testées, la listes des adaptations réalisées et les idées rejetées pour expliquer le processus dans un cas concret. Avec la liste des avantages à ne pas avoir lancé les développement avant cette phase de tests. Ensuite un powerpoint avec les système utilisé pour mesurer le retour des utilisateurs sur la fonctionnalités mise en place et les résultats à 3 semaines, 3 mois ...

**Cynthia** UX: 4 profils: Design - Coord - Real - Communication (et sensibilisation vers le C-level)

## Compétences

Pour faire parler les gens de profils différents il faut avoir un vocabulaire commun.

C'est super important de briser les silos - pas un UX manager qui ne parle pas aux autres - il faut diffuser

### Modèle en T

Répertorier par grande famille de métier les différentes compétences.

|Business|UX Research|Concept XP|Contenu|Architecture de l'information|Design d'interaction|Design visuel|Réalisation technique|
|-|-|-|-|-|-|-|-|
| | | | | | | | |

* **Vert** - maitrise parfaite
* **jaune** - connaissance junior
* **bleu** - a aquérir
* **blanc** - pas de compétences

Modèle en T: 
* Connaissance Globale
* Expertise pointue

Implémenter le modèle en T au travers de toutes les équipes afin de déterminer si l'on sait couvrir l'ensemble pour créer cette UX team.

### Equipe UX au sein de l'organisation

* **Centralisé**: Manager + équipe UX indépendante
    * Avantage
        * On peut aller très loin dans l'expertise
        * Bcp de ressources
    * Inconvénient
        * Centre de coût --> besoin de bcp de feedback et de com
        * Tomber dans des silos --> bpc d'évangelisation
        * Réduction de budget ==> pas possible d'être sur tous les projets

* **Décentralisé**: Des UX intégré dans les équipes produit au sein de la squad
    * Avantage
        * Impliqué de manière quotidienne
        * Responsabilité du PO/PM de l'impliqué
    * Inconvénient
        * L'UX à souvent des point UI et il devra répartir son temps
        * UX devient surtout de l'UI
        * On fait toujours la même chose, tjrs le même produit, peu de variété
    
* **Matricielle**: Equipes produits avec UX rattaché à l'équipe produit et à un UX manager transverse à l'organisation
    * Avantage
        * UX apporte à l'équipe produit
        * UX rapporte au manager de manière globale au manager
    * Inconvénient
        * Complexe à mettre en oeuvre, fonctionnelle et hierarchique

    Ex: UX dans une squad scrum puis par moment échange avec le UX manager avec guidelines et centralisation des demandes

> La grandeur d'un métier est avant tout d'unir les hommes; il n'est qu'un luxe véritable et c'est celui des relations humaines - 
**Antoine de Saint-Exupéry**

### Soft skills

* **facilitation**: ne pas avoir peur de parler en public, savoir écouter, gérer le temps
* **impovisation**: ça va toujours foirer à un moment ou l'autre, il va falloir savoir imporviser
* **planification**: savoir organiser les tests, inviter les gens, prévoir l'imprévisible
* **rigueur**: si on vuet de l'impact, sinon on n'est pas pris au sérieux - de la rigueur dans l'approche sa rassure les clients et donne confiance, on site ses sources, ses méthodologies, on explique pourquoi et on donne une idée de ce que ça va donner
* **empathie**: savori se mettre à la place, savoir écouter, savoir se mettre dans la peau -- pas de moi je ...
* **curiosité**: lire bcp, s'adapter en permanence (méthode en ligne, guerilla) - que se passe t'il chez facebook, adidas, ...
* **créativité**: la créativité ce n'est pas inventé de rien, c'est mettre des choses qu'on connait, les combiner pour créer quelque chose de nouveau

### RoI sur la conception centrée utilisateur

* On **valide** avec des faits
* On se **focalise** sur un produit qui a du sens pour l'utilisateur final
* **Epargner** des ressources, on ne développe plus des choses qui ne servent à rien
* **Motiver** l'équipe qui va dans le même sens au service de l'utilisateur

### Exercices - 5 minutes on change quoi

1. Matrice des compétences (T)
2. Création d'une UX team qui regroupe ses compétences et vérifier qu'au sain de chaque produit l'ensemble des compétense soit présent
3. Affiner et s'approprier la notion de compétence - lexique - glossaire et se mettre d'accord sur tous les termes
4. Communiquer l'avantage d'avoir cette matrice de compétence et impliquer les RHs dans le processus
5. Etablir les besoins annuels en consultance pour contrebalancer les manques dans les compétences
6. Faire le même travail sur les soft skills
7. Prévoir un parcours d'amélioration et de suivi et des objectifs individuels d'amélioration
8. Prévoir des moments déchange et de communication entre l'équipe UX et vers l'extérieur sur la veille et le suivi possible

## Définir son plan

1. **Cadrer** le projet centré utilisateur
2. **Décrire** ce que l'on sait des utilisateurs

### 1. Cadrer le projet

Canevas type

1. Motivation business
2. Périmètres
3. Echéances
4. intervenants
5. Utilisateurs
6. Risques
7. Contraintes
8. Hypothèses
9. Objectifs UX
10. Méthodologie
11. Livrables

### 2. Décrire ce que l'on sait des utilisateurs

UNIC canevas: User Needs Investigation Canvas

> quel que soit votre point de départ que veut l'utilsiateur

#### Proto persona

|Titre| type 1 | type 2|
|-|-|-|-|
|Caract socio-démo|rolex|50% H + 50¨femme entre 37 et 57 ans|
|Jobs to be done|entretien, réparer, régler|idem|
|Poids|750 gr | 500 gr| 
|Priorité|... | ...|

Canevas qui reprend l'empathy map, le value proposition + ...

1 canevas par persona - 1 canevas par profil

On pourra toujours fusionner après si très similaire

Chez Microsoft: Persona --> Placesona car le contexte est très important

1. **Topic**
2. **Caractéristiques**
3. **Context/Place/Lieu**
4. **Goals**: Besoin primaire aussi - why - why - why --> besoins psychologique
5. **Actions**
6. **Pain points**
7. **Motivations**: Besoins psychologiques
8. **Supporters**
9. **Alterenatives**: Alternatives possible et existante
10. **Solutions features**: 
    * solution existantes 
    * autres: à investiguer
11. **Solution obstacle**: peurs, freins, obstacles
12. **Value for persona**: valeur ajoutée, qu'est-ce que ça apporte

On a dans ce UNIC des hypothèse qu'il afudra aller tester ==> le document va vivre et évoluer. Hypothèse --> fait ou sont rejetée, de nouvelles hypothèses arrivent.

C'est un document vivant qui évolue au fil du projet

*Oublier la pyramide de maslow qui n'ait étayé par aucune preuve*

#### Besoins psychologiques

https://carinelallemand.files.wordpress.com/2015/12/ux-cards-instructions-v1.pdf

* Plaisir - Stimualtion
* Influence - Popularité
* Compétence - Efficacité
* Bien-etre physique
* Sécurité - Contrôle
* Autonomie - indépendance
* Relation - appartenance

## Exercice pratique - horloger - par table de 3

![Exercice Groupe 7](images/Screenshot_2020-09-25 ULB_Interview_groupe_7_montre.png "Exercice en groupe de 3 - Groupe 7")

### remplacer les croyance par des faits

* Méthodes génératives: on imagine 
* Méthodes évaluative: on demande aux gens
* Méthodes hybride: un peu des deux

**comment choisir une méthode**

|Urgence| Connaissance des utilisateurs basse | Connaissance des user importance |
|-|-|-|
|Elevée| | |
|Faible| | |

Utiliser la bonne méthodologie en fonction de l'urgence et de la connaissance

|Que| Qu'est-ce qui se passe? (quanti) | Pourqoi cela se passe-t-il? (quali) |
|-|-|-|
|Font-ils? (comportement)| | |
|Disent-ils? (attitude)| | |

Il faut croiser les données quanti/quali pour mieux comprendre


