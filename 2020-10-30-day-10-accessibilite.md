# Accessibilité

## Tools

- Jaws
- Silktide (chrome)
- NoCoffee (chrome) - trouble de la vision (daltonisme ...)
- SimViz (Android) - https://apkpure.com/simviz/com.humanpluslab.visimulation.simviz
- WebDisability Simulator (vue, parkinson...)
- [Funkify](https://www.funkify.org/?v=f003c44deab6)
- Inclusive design toolkit microsoft
- Talk Back (android)
- Voice over (iPhone)
- Daltonizer
- EyeView
- Tengo Baja Vision
- Windows (Win - +)
- Mac (Pomme - +)
- [NVDA](https://www.nvaccess.org/download/)

## To follow

* Alex Bernier
* Serge ... passe muraille

## Handicap

* Déficience motrice principalement des membres supérieurs
* Sensorielle (visuelle, auditive)
* Organique
* Intellectuelle/Mentale (concentration, dyslexie, Alzheimer ...)

* 80% des handicaps surviennent au cours de la vie
* 15-20% souffre d'un handicap permanent https://www.who.int/health-topics/disability#tab=tab_1 
* 10K Belgian = aveugle
* 900 K Belgian 65+ ans

### Problèmes rencontrés

* Afficher le password, tooltip... voir avec le touch est-ce possible, sans le mouse over ...
* Sous-menu au mouse over peut être très problématique pour les personnes en zoom fort (mal voyants...)
* Element d'activation des liste de cookie, idéalement les clicks dans les labels doivent pouvoir activer les boutons toggle ==> Toogle button à gacuhe du label

## Recommandation

**troubles de la vision**

Toujours autoriser le zoom 
mais aussi:
- agrandir les caractères 
- augmenter les contrastes

* règle braille max 80 caractères
* lecteur vocal

Toggle à gauche du label (liste de cookies ...)

**trouble dyslexiques**

Voir vidéo: https://www.youtube.com/watch?v=j9fsb91VcEA

- espace interligne suffisant reco: 1.5 em
- justification peut poser problème car les espaces ne sont pas identiques
- Majuscule / Minuscule = même conseils
- Fonts: minimum 13 + option d'agrandir des caractères sans superposition jusqu'à minimum 200%

**tremblements**

Bouton minimum 44px de haut et 8px de distance
Rendre toute la zone cliquable

**trouble de l'attention - disctraction**

* Bien délimiter les zones
* Titres clairs
* Pas trop de texte

    