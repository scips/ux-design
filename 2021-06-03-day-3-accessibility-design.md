# Design

Outils: tout le monde converge sur Figma

Figma, super facile pour les clients externe, plugin facile, grosse bibliothèque


## Exercice d'introduction

Choisir un personnage:
https://www.gov.uk/government/publications/understanding-disabilities-and-impairments-user-profiles

https://www.gov.uk/government/publications/understanding-disabilities-and-impairments-user-profiles/simone-dyslexic-user

Simone dyslexique 41 ans 2 enfants 1 mari

Lire la fiche du persona et noter ce qui étonne
* Diagnostique tardif
* Elle a trouvé des solutions (audiobooks ...)
* Lorsque ça bouge ou que c'est trop proche ça dérange (avec les banners et les ads en plein milieu de chaque article ça ne doit pas être facile) - effort supplémentaire lié à la concent
* Justified text ??? --> parce que la distance entre les mots varient
* Italic ??? --> pas d'abus
* Timeouts (2FA)
* 10% dyslexia
* 1/6 adult has a reading level of 11 years old
* Elle semble avoir un trouble de l'attention en plus
  * Parfois un seul aspect masque d'autres aspects

## Design d'interaction

Il faut définir une stratégie globale d'accessibilité --> responsable accessibilité

### Persona pour le design d'interaction

1. C'est bien d'avoir des persona de toute façon
2. On filtre ensuite en fonction du besoin
3. On retombera alors sur un pattern commun

### Skip link - Saut de navigation

Permet d'aller directement au principal

Visible à la tabulation

A cacher au chargement de la page

On peut le placer n'importe ou

Il doit être lisible en fonction de l'endroit

* Skip to content
* Skip to navigation

### Focus

Focus et zone cliquable bien visible.

### Sections claires

#### nav

On peut avoir plusieurs landmarks **nav** et qualifié le landmark
* Navigation: fil d'arianne
* Navigation: catégorie
* Navigation: plan de site ou footer

* Navigation: menu
  * AAA : retirer le lien vers la page en cours

* Navigation: paging
  * Pour la pagination, c'est important de ne pas avoir qu'un seul facteur pour indiquer la page en cours: exemple changement de couleur + bloc autour
  * AAA : retirer le lien sur la page en cours

* Navigation: carrousel
  * Fleches
  * sinon puces 44 px et 8px déloignement
  * Pas de swipe sinon le lecteur d'écran change d'application
  * Carrousel arretable au clavier (et à la souris ou au touch)

##### Focus

* Focus personnalisé
* Focus de tout ce qui est clickable
* Un style pour le Focus

Ne pas retirer le style a:focus

##### Hover

* Faire un style différent du Focus

##### Liens

Avec suffisamment de contraste entre le texte, le fond et le lien

##### Majuscules

Les majuscules sont bienvenues dans des zones de texte courte

##### Fonts

* On recommande le sans-serif pour le web en terme d'ergonomie
* Pas d'obligation mais rendre le style écrasable
* Taille minimum 16px - ici les outils reprennent le lead
* idéalement rem, em, % mais il doit être possible pour l'utilisateur de changer la taille de la police dans les paramètre du navigateur ou du système --> supporter jusqu'à 200
* Il faut donc indiquer aux devs les limites du système en cas de zoom
* line-height en unité relative
* line-height: tout doit fonctionner avec line-height 1,5; word spacing 0,16 rem; ...
* Seuils gras/nongras

| norme |type | petit | moyen | grand |
|-------|-----|-------|-------|-------|
| AA | gras  | 4.5   | 3     | 3 |
| AA | non gras | 4.5 | 4.5 | 3 |
| AAA | gras  |  7 | 4.5     | 4.5 |
| AAA | non gras | 7 | 7 | 4.5 |

le **gras** c'est min 700 font-weight

* Ne pas se faire confiance pour les contrastes, toujours faire valider par un outil

* Opacité ok mais au focus, supprimer l'effet

###### Palette de couleur

Créer une palette de couleur avec les contrastes suffisamment élevés pour fournir aux équipes de dev, comme base générale

###### Image et texte on top

* Prévoir une image de fallback (background quand l'image n'est pas chargée)
* Retirer l'image et vérifier que c'est lisible au hover
* Séparer le texte de l'image et assurer du contraste
* Dégradés: vérifier le contraste de la pire des couleurs

###### Elément inactifs

Pas dans les WCAG mais faire un effort ne coute rien, sinon enlevez les de l'écran

###### Statut

Le contraste de statut est de **3** entre les 2 statuts

Par exemple dans un formulaire ou du ouvert fermé

Focus dans une liste déroulante

###### Copy-writing

En cas d'écriture inclusive écrire avec des points médians. **acceuillant⋅e⋅s**

### Toujours 2 façons d'atteindre les pages

* Via le menu principal
* Par le moteur de recherche
  * ou Un plan de site (pas full) avec la même organisation que le menu principal
  * ou Un fil d'ariane

### Mise en page pyramidale ou en pyramide inversée

![Pyramide inversée](images/pyramide-inversee.jpg)

### Call center

Interaction avec les utilisateurs nécessite un accompagnement particulier pour les seniors (pas d'interaction avec le clavier possible).

### Autocomplete

* Moins de fautes

### Timeout

![Pouvoir accorder plus de temps](images/besoin-de-plus-de-temps-decathlon.png)

1. Annoncer le timeout
2. Permettre de le prolonger

### Formulaires
Labels et input très clairs pour les logiciel pour aveugle

label bien identifiés et idéalement dans la zone

placeholder à éviter et mettre l'info en dessous

erreur doit être en dessous

### Tableau

Lignes horizontales pas indispensables mais bien veiller à avoir 

### Message de cookie
* Mettre un headings

### Déclaration d'accessibilité

* obligatoire
* il existe un template
* exemple: https://help.netflix.com/fr/node/116022

### icones/images

* enlever les zones dégradée pour identifier la lisibiltié
* lier les zone de texte aux éléments de l'image le plus possible

### icon font

* Ok bonne idée

### flash et clignotement

* maximum 5 secondes

### portrait + landscape

imaginons les GSM monté sur un bras, si on doit tourner le téléphone, ce n'est pas bon. Exemple vidéo qui passe automatiquement en full screen.

## Design Visual

Se baser sur material design

Expliquer et designer l'ensemble des écrans avec les cas de zoom (screen magnifier)



## UX

### Mobile

![Outil d'accessibilité android](images/accessibilite-application-mobile-android.png)
Tester avec les outils d'accessibilité sur téléphone

En mobile la zone de click représente enter 57 et 72 pixels

Rotation = nouveau layout

Attention le swipe permet de passer d'élément prononçable en élément prononçables



## Tools

* Figma addons: 
  * Adee Comprehensive Accsibility tool - taille de bouton, alt text, ....
  * getstarck.co
  * figma contrast
  * figma "Able"
  * aditus
* Tota11y (totally) - contraste
* Accessible-colors.com
* Tanaguru constrast finder
* snook ca
* webaim.org/resources/linkcontrastchecker/
* contrast-triangle.com
* contrast-ratio.com (supporte la transparence alpha channel)
* RGBA to RGB connexion
* color.madiaandme.be
* colorsafe.co
* contrast-grid.eightshapes.com
* a11y-style-guide.com
* brandwood.com/a11y serait bien si il gérait aussi la notion de zoom et d'agrandissement du texte
* Stylish
* trace.umd.edu/peat
* Figma Accessibility annotation
