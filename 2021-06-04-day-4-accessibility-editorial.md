# Editorial

## Référent Accessibilité

@ la RTBF important de mettre ça en place --> ComEx

## Navigation

### Qu'est-ce qu'un titre accessible? C'est un titre qui est aussi SEO friendly

* Ce qui distingue la page: H1
* Le nom du site doit être à la fin (pas au début)
* Titre unique par page
* Retirer les mots vides (le, la, du, des ...)
* Titre cours

Ex: Infos de voyage | Brussels Airlines

Contre exemples:
* Bienvenue sur le site du CPAS .... bcp d'article vide
* Betrix - acceuil extrascolaire ... dans le mauvais sens --> Accueil Extrascolaire - Bertrix

### Saut de navigation

* Aller au contenu

Si 2 sauts

* Aller au contenu
* Aller au menu

### Menus de navigation

Landmarks pour sauter des sections de la page

Attention si plusieurs nav. En tant qu'éditeur on doit prévoir les textes à mettre dans l'alternative.

**Attention: !Navigation en dehors du main dans la partie header (ou footer)!**

**Si nav dans le main, nav lié à l'intérieur de l'article**

**nav**:
* Principale
* secondaire
* Vous aimerez peut-être
* Vous êtes ici
* Page précédente/suivante
* Liens d'évitement
* Dans cette page
* Vous êtes ici? Fil d'ariane - breadcrumb

Demander de rajouter les titres des widgets dans les landmarks le titre (role ou autre)

### Titres

Navigation par titres moins rapide mais autre façon de naviguer

Utilisation des catégories en début de titre - donne l'avantage de pouvoir skipper en entendant les thématiques 

Utiliser les titres pour accéder au menu principal est une très très bonne pratique

**Remarque: Attention retirer les H3 sinon c'est trop dense, ce n'est que pour du référencement et pas pour l'accessibilité**

Voir par rapport à la BBC, The guardian ... 

Titres en majuscule, bien mettre les accents

### Liens

Eviter les articles dans les listes de liens

Les liens doivent être clair à l'exception des "Voir plus"

La personne aveugle va entendre toutes les lettres si on indique une URL

==> Interdiction d'avoir des liens sur les urls alors on met une alternative (title) et indiquer aux ITs ce que la personne aveugle va entendre

Attention ne surtout pas mettre de titre si le texte du lien est déjà clair

On peut utiliser les titles pour préciser qu'il s'agit d'une ouverture d'une nouvelle fenêtre

Attention le title est lu À LA PLACE du contenu



Ex: 
```html
<a href="https://www.w3c.org" target="_blank">Aller sur le site du W3C</a>

<a href="https://www.w3c.org" target="_blank" title="site W3C - Nouvelle fenêtre">Aller sur le site du W3C</a>
```

Bien décrire le lien surtout lorsqu'il peut y avoir confusion dans la page (exemple: rapport --> rapport d'activité 2018, rapport de présence 2018)

### Langue

Langue de la page

Langue des extraits dans la page (pour des termes dans d'autres langues)

```html
<span lang="fr">Bonjour</span>

<span lang="en">Heelo</span>
```

### Titres et sous-titres

Utiliser les titres clairs plus en style journalistique

H2:La mammographie --> H2:Pourquoi passer une mammographie?

### Contenu

* Objectif en début de phrase (pour pouvoir skipper)
* Restez concis et directe "Dans la mesure du ..." --> "Pour ..."
* Expliquer les abréviation lors de la première présentation d'une abbreviation ex: W3C --> W3C (World Wild Web Consortium)
* Ecriture inclusive à prévoir dans le CMS avec le point médian + communication sur la rédaction (powerpoint)
* Pas de références à des styles (voyez ici en italiques les termes ... c'est impossible pour eux (ou très difficile) par conter ci-dessous passe)
* Vocabulaire
  * Idéalement pour les enfants de 11 ans -> type écriture web, simple concis

Exercice: scolarius

texte:
> Près de 820 millions d’euros de contrats liés à la lutte contre le Covid font l’objet d’enquêtes pour corruption en Afrique du Sud, y compris une affaire liée à deux proches du ministre de la Santé, Zweli Mkhize.
> "Nous sommes à un stade critique de l’enquête. Je ne peux pas entrer dans les détails […] Nous enquêtons sur toutes les accusations. Le ministre a exprimé sa coopération", a dit mercredi lors d’une session en ligne au Parlement, le chef de l’Unité des enquêtes spéciales (SIU), Andy Mothibi.
> Visage de la lutte contre la pandémie et promis à un brillant avenir au sein de l’ANC, parti historique au pouvoir, Zweli Mkhize est dans la tourmente : la semaine dernière, plusieurs médias ont dévoilé que son ex-porte-parole, Tahera Mather, et son ancienne assistante personnelle, Naadhira Mitha, auraient touché des millions de rands en paiements irréguliers liés à un contrat de communication autour du Covid-19.
> Le ministère a reconnu "des irrégularités flagrantes auxquelles il faudra certainement répondre", réclamant "un peu de temps", ce qui lui a valu de nombreux sarcasmes.
> Les enquêteurs épluchent plus de 4000 contrats portant sur des services et fournitures liés à la lutte contre le coronavirus. Environ 40% des contrats suspects ont été examinés, a précisé M. Mothibi. "Nous continuons à recevoir de nouvelles allégations de mauvaise gestion et d’irrégularités", a-t-il ajouté.
> Son unité, sous la tutelle du ministère de la Justice, s’intéresse depuis l’an dernier à des dizaines d’entreprises auxquelles ont été attribués de juteux contrats dans le cadre de la lutte gouvernementale contre la pandémie.
> Jusqu’ici, plus de 60 fonctionnaires ont reçu des sanctions disciplinaires et 87 entreprises ont été inscrites sur une liste noire.

Score 147 descendre en dessous de 100

Utiliser des outils de type: Antidote 10 à intégrer au CMS par exemple

[Voir aussi Référentiel rédaction langage simplifié - disic](https://disic.github.io/referentiel_redaction-langage-simplifie/)

### Déclaration d'accessibilité

**Pas dans les WCAG - mais dans les directives européenne**

* Organisme: RTBF
* Couverture: rtbf.be/ ; app RTBF news; app RTBF Auvio
* Standard de conformité: WCAG 2.1 AA
* Quelle méthode a été utilisée pour vérifier l'accessiblité: audit par l'institution ou un tiers (lequel)
* Etat de la conformité: totale, partielle ... et les exceptions
* Liste ou type de page accessibles
* Raison de l'innaccessibilité
* Date de la première déclaration
* Date de dernière révision de la déclaration

BOSA
Equal
DNA

VAlider avec l'outil BOSA: https://accessibility.belgium.be/fr

### Images

Si non décorative prévoir un alt et le alt doit décrire l'image de manière très succinte sans article

idéalement avec des légendes: figcaption

Si élément de décor: mettre des alternative vide

* alt et title ???
  * Si ce sont les mêmes ça ne sert à rien
  * Si ce ne sont pas les mêmes les deux sont lu et ils doivent être complémentaires

#### Diaporama

Photo 1 sur 3, Photo 2 sur 3 ...

#### Graph

Détail de ce qui est dans les statistiques ou un contenu spécifique pour les personnes aveugles

### Vidéos

Les vidéos publiées après 2020 doivent avoir des alternatives textuelles caption

### Formulaires

label synthétiques

boutons clair (pas OK mais bien Valider, envoyer, s'inscrire ...)

### Table

Titre des tableau avec caption

C'est important d'imaginer que la personne va pour chaque cellule avoir le heading de colonne et de ligne relu avec le contenu de la cellule.

### Mobile

#### image

Si l'image n'est plus associée à un titre (en mode mobile il faut mettre le caption ou l'alt)

Faire les tests complet sur les applications avec le voiceover ou talkback d'android

### Exercice

* Image décorative: alt=""
* Image bouton: alt="action du bouton"
* Image lien
  * Image seule: alt="intitulé de la cible"
  * Image + texte: l'image complète le texte avec une précision exemple "PDF" ou alt="" 
* Image d'une information redondante: alt=""
* Image d'une information unique:
  * Courte: alt="titre du contenu de l'image" ex: alt="george washington" ou mieux figcaption="george washington" + alt=""
  * Longue: alt"description + référence pour avoir plus de détail" ex: graphique

* Image descriptive d'un article avec un lien pour zoomer => un lien
* Image de partage sur twitter

https://www.a11yportal.com/good-bad/home.html

Good: Skip to main content
Good: switch de langue en anglais parfait
Good: Les tabs font bien des navigation de sections
Good: Les liens sont bien lu avec le contenu du lien et pas l'url
Good: les zone de régions sont bien identifiées (landmark)
Bad: Pas de langue lecture du contenu ave la langue en français
Bad: Read more ... partout
Bad: Navigation par titre impossible


## Références

* Liste des labels anais digital comme base
* Deque University VoiceOver & NVDA documentation "voiceover shortcuts"
* Web developper toolbar Chrome & Firefox
* Vocabulaire
  * https://translatedlabs.com/text-readability
  * https://www.scolarius.com/
* https://disic.github.io/guide-lecteurs_ecran/nvda.html
* Français simplifié FALS

## Todo

* Powerpoint
  * Accesiblité kesako
  * Empathie
  * Thématiques
    * Editoriales
      * Point médian pour l'écriture inclusive
  * Tests
    * Composant par composant
    * Avec le matériel adequat NVDA - VoiceOVer
    * A chaque fois en CI/CD



