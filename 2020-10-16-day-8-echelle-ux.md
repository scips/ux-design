*by Guillaume Gronier*


### Biais

Pas de biais lié aux tests avec les mêmes utilisateurs. Le seul risque est un positivisme exagéré mais qui indiquera toujours une bonne évolution du produit.

### Population

#### Pour les tests utilisateurs

5 utilisateurs = 80% des problèmes 

#### Pour les questionnaires

5 utilisateurs c'est bien on a un score mais statistiquement pas fiable

Si on veut faire des statistiques correcte

Tests: T2student, lanova pour déterminer si il y a une différence significative ou pas.

30 personnes par questionnaire c'est le minimum

Si on veut déterminer si des utilisateurs sont en dehors ==> régression linéaire + nuages de point ==> Clusterisation

Il faut choisir si l'on garde ou pas les utilisateurs sont complètement à côté de la plaque identifier au cas par cas

## UX

L'ux peut être représentée par le modèle CUE (Component of User Experience) [modèle de Mahlke](https://www.researchgate.net/figure/The-CUE-Model-Components-of-User-Experience_fig1_221515974)


# Analyse des émotions

## PrEmo

## Plutchik

![roue des émotion de Plutchik](images/Plutchik-wheel_fr.svg)


Voir aussi:
http://www.guillaumegronier.com/cv/resources/EchellesUtilisabiliteUX_v1.pdf



