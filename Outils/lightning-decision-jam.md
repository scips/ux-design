# Lightning Décision Jam

**Host**: CapDegimi - Pierre Hayon, Mehdi En-Naizi, Eloy Palacios, Coralie Villeret, Mélanie Vanloock, Sébastien Barbieri

* Durée: 1h > 30 minutes (si ça tourne bien)
* Cible: Innovation
* S'inclut dans: Design Sprint
* Type: Rétrospective (comment améliorer ou inventer ce qui n'existe pas encore)
* Complexité: Easy

## Histoire

German - Edge and smart - meilleur agence d'innovation

## Utilisation

**Toutes les entreprises** pour l'**interne** au moins ou pour **enseigner** à des équipes pour remplacer un tas de réunion.
* Faiblesse des rétrospective: on peut arriver avec des idées sans ownership après - rebondir est plus difficile --> Lightning décision jam
* Population: Designer oui mais surtout des personnes qui n'ont pas d'aprioris - marketing, études, product design
* Remplace des réunions
* Facile et accessible

### Soucis

* Design Sprint Stratégie - ce qui marchait le moins bien c'était les points trop ouvert et l'exercice à permis de solutionner ceci
* Business Analyst PDF - permet de ritualiser ce que chacun faisait de son côté -> langage commun, réduction du nombre de meeting

## Process

1. Ce qui fonctionne bien
   1. 4 minutes - musique de 4 minutes + sonnerie
   2. Mise en colonne (nbr de colonnes = nbr de participants)
   3. Chaque personne lit une colonne
2. Ce qui freine - tout ce qui ne fonctionne pas
   1. 6 minutes - musique de 6 minutes + sonnerie
   2. Rien de personnel
   3. Mise en colonne
   4. Chaque personne lit une colonne
   5. Clusteriser
   6. Vote + Decider Vote - le design n'est pas une démocratie et ce n'est pas la décision du designer qui compte - on peut twister le vote pour que ça aille plus vite - vote du décideur peut être pratique (5 votes pour le decider et 3 votes pour les autres)
      1. Voter maximum 2
      2. 3 minutes musique + sonnerie

Pourquoi toujours commencer par ce qui est positif?  Et pourquoi moins de temps?
* D'abord on est positif car on aime ce qui est positif
* Clayton - Jobs to be done - ce n'est pas en donnant aux gens ce qu'ils veulent mais en résolvant leurs problèmes
* Ennemi de l'innovation: le débat, la discussion - problème d'introversion, extraversion, débat oral les gagnants sont ceux qui ont le plus d'aplomb même si les idées sont pourries
* Méthode d'accompagnement
* Musique: si il n'y a pas de musique, les gens ont tendance à se parler - parler ici n'est pas utile - ça nous concentre
* Decider vote: donne bcp de poids et allège le process

Comment gérer les idées qui pop - en cours - on peut rajouter dans le "ce qui ne fonctionne pas"

Idées similaires: déduplication et regroupement avant vote

### Parking lot
Idée en plus - mais hors scope ou tout ce qui n'est pas retenu --> Parking lot

### Top 3 des idées + Reframe

Par priorité les plus de vote aux moins de vote (top 3)

1. Relecture des points
2. Identification des axes
   1. Stratégie
   2. Technologies
   3. Recherche - UX
   4. ...
3. Choix du points le plus facile à traiter --> The problem
4. Transformer les phrase en: How Might We? Comment pouvons-nous?
   * Comment peut-on rendre la plateforme plus accessible?

### Idéation sans discussion

1. Trouver un maximum d'idée
   1. 6 minutes - musique
   2. Titre bien visible: "Comment peut on rendre la plateforme pus accessible?"
   3. Pas la peine de trouver l'idée qui va tout casser
   4. Privilégier la quantité à la qualité
   5. Concentrez vous sur: vidé votre cerveau concernant votre produit
   6. Pas trop court, pas trop long (pas de pitch après) - idée pas claire --> on éjecte
   7. Mise en colonne + lecture à tour de rôle de chaque colonne
   8. Clusteriser les idées
   9. Voting
      1. 1-4 points par personne par idée

Pas trop dans le détail, c'est bien, pas de la solution très fonctionnelle, mieux

### Prioritiser la solution + Effort / impact

Et si ces solutions étaient misent en place, c'est le dernier qui parle qui a gagné

1. On travail sur l'impact estimée - est-ce que ce serait: très faible, incroyable, moyen

On est dans un monde de projection, on ne doit pas justifier ou expliquer

