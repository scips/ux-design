# Enquete et benchmark

*Régine Lambrecht*

## Benchmark

Analyse de la concurrence

### Pourquoi?

* Se positionner face à la concurrence
    * Bonnes idées
    * Intéressante pour nous
    * Identifier les erreurs de la concurrence
    * Inspirer

### How?

* Qu'est-ce que je veux apprendre? Comment le contenu est organisé? Quels sujets sont présents?
* Etude de la concurrence sur le nombre de concurrent qui ont fait certains changements.

### Preparation

1. Exploration: chercher les concurrents de même niveau - quels **besoins** ils couvrents - quel est leur **cible** - **faiblesse** des concurrents
2. Idéation: chercher les points originaux - les variantes - sortir du marché pour s'inspirer
3. Génération (converger vers des solutions déjà évaluée): comment les problèmes sont **résolus** - matrices décisionnelle pour évaluer la faisabilité, le couts et la renconter du besoin avec experts techniques
4. Evalutation: **tester** + utiliser des **échelles UX** + **illustrer** les recos d'audit

#### Périmètre

**Combien d'exemples comparer?**

* A partir d'un moment on retrouve toujours la même chose
* Diviser le travail sur des thématiques à travers tous les concurrents:
    * Réactivité du système
    * Emotion
    * Architecture

**Trouver les concurrents**

Qui fait des choses similaires? Qui fait une sous-partie de votre métier? Chercher chez le client et chez les utilisateurs.

Chercher dans les forums uniquement pour trouver les utilisation alternative.

Etudes comparatives NNGroup, Gartner, ...

https://www.applyzer.com/main.php?mmenu=worldcharts

#### Trouver des exemples

* Faites tout le temps des screenshots des différentes interfaces (réservation de billets, banque ...)

* Canada, allemagne, australie, Bresil (accessibilité), Norvege, suede, finlande, portugal (accessibilité)

* https://goodui.org

### Analyse

**Classement**

* Ne pas chercher à trouver un vainqueur = perte de temps
* Ne pas lister tous les points faibles = perte de temps

* Identifier les opportunités et les risques
* Identifier les points forts

**Exhaustivité**

## Exercice

Par rapport aux montres.

Quels étaient les problématiques?
- Service à domicile
- Marque spécifiques
- Trouver le bon
- Devis
- Logistique - Sécurité du transport
- Testimonials

**Pour l'exercice** quels sont les sources pour trouver des réparateurs, identifier l'agréement, la compétence, les avis - testimonials

1. Via google - je cherche une réparation montre, frigo, ...
2. Via les sites de marque - 

## Déterminer les profils d'acheteur

http://blog.extendeddisc.org/disc-types-different-buying-decisions

DISC buyer profiles


### Comment structure l'information

* Lister les sites

|URL|type|
|-|-|
|https://support.apple.com/fr-be/watch/repair/service/pricing| Support d'apple officiel|
|https://www.isolus.be/reparation-apple-watch||
|https://fr.ifixit.com/Tutoriel/Remplacement+de+l%27%C3%A9cran+de+l%27Apple+Watch/41082||
|https://www.ateliernet.com/index.cfm||
|**https://www.hometime.fr/**|**Service à domicile**|
|https://help.switch.be/hc/fr-be/articles/360022072792-R%C3%A9paration-d-Apple-Watch||
|https://www.captain-repair.be/reparer/apple-watch/apple-watch-42-mm/||
|https://misterminit.eu/fr_be/r%C3%A9paration-de-montres|réparation horlogerie|
|https://www.starofservice.be/annubis/bruxelles-capitale/region-bruxelles-capitale/bruxelles/reparation-de-montre | |
|https://www.artisandutemps.com/reparations| |

* identifier ce qui peut rassurer

#### Architecture

* Sitemap
* Comparer avec le menu
* Répertoires physique du site
* Ce qu'on trouve dans google

#### Lister les screenshots avec ce qu'il faut retenir

!(Home page hometime)[images/Screenshot_2020-09-29 Hometime-01.png]

* Aspect confiance et sécurité
    * Numéro de téléphone + adresse
    * Liste des marques (beaucoup de marques renommées)
    * Photos haute qualité
    * Être recontacté
    * Tranche claire pour les devis
    * 

## Enquete

A large spectre sur un échantillonage large, pour obtenir du Quali et avoir des chiffres.

* Tri de cartes: on donne des cartes et les gens doivent trier sur ce qui est le plus logique
    * contenu
    * contenu et modèle de navigation

--> Quanti

* Attrakdiff: questionnaire en ligne à utiliser en fin de parcours

--> Quanti

* Une seule simple question en fin de parcours

--> Quanti

* Net promoter score: recommanderiez-vous ce site à un ami? 1-10

--> Quanti

* Questionnaire de satisfaction

--> Quanti / Quali

**Avantage** - voir slides

**Inconvénients** - voir slides - max 8 à 10 questions - biais (plutot des femmes en général) - nécessite l'aide d'un statisticien

### Préparation

* Peux nécessité des interviews quali pour préparer. 
* Identifier le ciblage vers la population à atteindre aussi
* Garder une question ouverte à la fin au cas où, pour abandonner le questionnaire et en refaire un autre

#### Lean survey canvas

* [Lean survey canvas](https://www.invisionapp.com/inside-design/how-to-create-a-survey/) (que voulez-vous apprendre - quels informations avez-vous déjà)

* Utiliser les sources analytics pour préparer
    * croiser avec les données quanti pour limiter le plus de possibilités
* recrutement pour éviter les biais (femme avec enfant en sur nombre)

* **Cible**: combien de personne interroger? [SurveyMonkey](https://www.surveymonkey.com/mp/sample-size-calculator/) + [Jeff Sauro - A pratical Guide to Measuring Usability](https://www.amazon.com/Practical-Guide-Measuring-Usability-Quantifying/dp/1453806563)

idéalement un degré de 80% de confiance est suffisant - *Jeff Sauro*

marge d'erreur en UX 19% d'erreur est acceptable - *Jakob Nielsen*

Attention en fonction du quetionnaire, si l'on fait un split on divise le nombre de répondants très rapidement, on peut donc avoir des réponses en bas de questionnaire qui sont trop peut représentatives.

Plus il y a d'arbre et d'élément dans le questionnaire plus il y aura d'erreur possible.

### Où faire l'enquète

* Pas à la première page
* Banner
* Fin de funel
* en sortie lorsque la souris quitte? (Mobile???)
* SMS
* Réseaux Sociaux
* Pop-up...

### Comment trouver les gens, comment les motiver

* Newsletter?
* Adapter le timing par rapport à la saisonnalité (sport, accouchements, ...)
* en fin de process
* déclencher en fonction du nombre de pages vues ou de temps de passage ... 
* identifier les patterns et décider quand le mettre en place en fonction

### comment augmenter le taux de réponse

* Carotte - réduction... à mettre dès la première page
* Attention aux risques de réponse aléatoire et aux concouriste (food concours par exemple)
* Analyser les zones de clic pour voir si il y a un billet

> Le comportement c'est la motivation, l'attitude et le déclencheur.

> Behavior = Motivation x Attitute x Trigger

> B=MAT

Si vous écrivez la première question dans le mail, il y a plus de réponses (avec directement deux boutons). *Jeff Sauro*

Garder les infos personnelles pour la fin (pas de blocage et nous avons déjà toutes les réponses)

Mettre en avant la renommée de l'enqueteur (ULB, UCL ...)

Relancer régulièrement l'enquète, analyser si trop de répondant, cibler un autre public

### Outils

* Au minimum un extract via CSV
* Simple d'utilisation ou puissant
* Payant en général

### Structure

* questions optionnelles le plus possible
* je ... lorsque je ...
* randomiser l'ordre des questions
* randomiser les réponses si possible
* questions globales d'abord

Taux d'abandon élevé après 8 questions

### Utiliser l'échelle de Likert

https://fr.wikipedia.org/wiki/%C3%89chelle_de_Likert

* Extrèmement **méchant** - Extrèmement **gentil**
* mieux: Pas du tout **gentil** - Extrèmement **gentil**
* 1 [ ] - 2 [ ] - 3 [ ] - 4 [ ] - 5 [ ] (impair laisser les gens rester neutres)
* Single easy question: préférer les mots aux échelles - Est-ce que c'était facile de 1 à 7 (1 est-ce facile ou 7 est facile ???)

### tester

Ne pas avoir de proposition limitante culturellement, certaine personne choisisse un acteur parce qu'ils ne connaissent pas les autres

### Biais

* Différence entre la désirabilité sociale dans l'espoir de se faire bien voir
    * revenus
    * sports

### Exercice

A faire avec le client de l'horloger

### Analyse des réponses

Utiliser plutôt **Mode** et **Moyenne** que Mediane

Mesurer les ecarts, la divergence, l'écart type

Profilage

#### Visualisation des données

* Graph par personna

#### Présentation

Une histoire par slide

