# Design Studio

*A Dehon*

* Découvrir
* **Conceptualiser**
* **Designer**
* Tester
* Développer
* Ecouter

## Méthode

* Brainstorming


Utilisation des biais de groupe pour avoir un groupe qui se challenge et qui converge

L'objectif est d'avoir un consensus sur une idée, une solution

Le Rôle de l'UX est de démonter les jeux polititque où les gens trop fermer sur leur solution

Taille: 5-10 participants

* En ligne: réalisable mais pas avec le même niveau de qualité

## Team

* Mandatory
  * UX researcher qui vient avec les insights + personas
  * Facilitateur - Timekeeper
  * Business (porteur de projet)
  * Tech/Dev/Architecte
  * Designer  qui va réaliser les wireframe
* Optionel
  * Key users


Chaque rôle est garde fou pour son domaine spécifique

## Organisation

### Espace

Spacieux, lumineux, Tableau blanc (ou sur mural.co ...)


### Convivialité

* Bonbon
* Fleurs

### Matériel

* Persona
* UX research
* Tableau blanc
* Règles affichée
* Kit de sketching

### Créativité

### Programme

* Cadrer le temps
  * Cadrer les temps de parole pour présenter
  * 

### Regles

* Pas de limite
* Pas de négativité
* E.L.M.O.
* Rebondir sur les idées des autres

## Design Studio

Design Charette - la charette pour rendre les travaux au dernier moment, juste avant que la charette ne passe ==> expression *"être charrette"*

==> Générer un maximum d'idée dans un temps limité

Prévoir le temps nécessaire

## Stimuler la créativité

* Positive emotional granularity
* UX CArds
* What if cards

### Déroulé

**UX Researcher Lead le déroulé**

1. Commencer par décrire ce qu'on sait des utilisateurs, expliquer ce qu'il est, mettre tout le monde en empathie
2. Brief design (30') 
  * description du persona
  * description de la user journey
    * Awareness
    * Consideration
    * Decision
    * Action
    * Service (période pendant laquelle le service est réalisé)
    * Retention (loyalty, relation long terme, garder le client conscient que le service existe toujours)
  * description des contraintes (MVP, time, platforme)
  * description de l'objectif
3. Réfléchir a des solutions pour chaque étape (comme dans le mental model)
  * Présenter le benchmark concurrentiel
  * Utiliser les what if cards pour explorer les solutions
  * Utiliser les UX cards pour identifier les catégories de sentiments
  * Identification des touch points lié aux solutions
4. Priorisation
  * Identifier le ratio Impact Business + Effort de réalisation
    * Système de matrice à gomette
      * BV
      * SP
        * Faisabilité
      * Risques
        * Environnement
        * Juridique
  * Défendre la valeur du point de vue utilisateur
5. Synthétiser

Boucle avec inner boucle dans la partie iteration

#### **Etape 1: Definition** 15'
Se mettre d'accord sur le flux et les écrans

#### **Etape 2: Generation** 5' - 10'

Seul ou a 2 - avec un canevas où l'on rentre dans les détails d'un point de vue graphique - utilisation de 6 to 1, canevas classique

#### **Etape 3: Mise en commun** Pitch

#### **Etape 4: Iteration convergente** 15'

Iteration

vote + affinage

dotvoting sur les écrans ou sur les fonctionnalités

Redessiner les écrans avec le feedback entendu juste avant. On s'arrète quand on n'a plus rien à enlever.

##### Risques

* Compromis à la Belge
* Prendre toute les idées

##### Méthode Corse pour itérer

UX Researcher de blablacar

**CACHER:** *J'ai mon écran qu'est-ce que je peux cacher?*

En enlevant le rating dans la liste de recherche dès le premier écran, peu de conversion, ça figeait les gens

Idem pour l'inscription à la newsletter, ce bandeau prend plein de place, peut-on le cacher

**ORGANISER** *Ce qui est important en avant et ce qui est moins important en arrière*

Plus grand, plus haut ce qui est le plus important

**REDUIRE** *Ce qui est moins important en plus petit*

Réduire les infos nécessaire mais moins importante

**STANDARDISER** *Quels sont les models mentaux connu des utilisateurs*

Barre de rechercherche en haut à droite = réflexe utilisateur

Utiliser le design system = contrainte = grande aide = on va plus vite

**ELIMINER** *C'est quoi l'objectif ce cet écran?*

Landing page, c'est expliquer le principe du service, tout ce qui ne sert pas l'objectif, on n'en a pas besoin.

Le formulaire pour décrire la panne, pas besoin d'inscription newslettre ... 1 écran = 1 objectif

#### **Etape 5: Synthèse** 15'

Synthétiser les idées 

#### Identifier la désirabilité, la viabilité, ... du résultat final + recommencer?

* On recommence à l'étape 1 si nécessaire puis on passe au wireframing + prototypage


## Tools

UXPressia (user journey, persona)

## Exercice

### Présentation du persona

*Jeff*

![Jeff](persona-day-6-design-studio.png)

### Présentation de la user journey

*User journey*

[User Journey PDF](user-journey-day-6-design-studio.pdf)

#### Rajout de solution

Laisser les gens venir avec des solutions

### Présentation des contraintes

1. Une application android/ios déjà existante
2. Que des dev d'application disponible

### Présentation du flux d'acran

1. Landing page
2. **App: accueil**
3. **App: formulaire**
4. **App: suivi**
5. Compte
6. Ma montre

#### Rajout/modification ds fonctionnalités disponible

- App accueil
- 