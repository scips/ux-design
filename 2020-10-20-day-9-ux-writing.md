# UX Writing
*Régine Lambrecht*

## Stratégie éditoriale

Mot magique: justifier sa demande avec **parce que** permet d'améliorer la conversion. (Harvard 1977 - je veux utiliser la photocopieuse)

* Attention
* Interressé
* Désir
* Agir

## Content first

Itérer:
1. Carte de contenu réaliste
   1. Travail sur les sitemap
   2. Récupération des templates et pages html
   3. Récupération des DB
   4. Workflow de la création de contenu
   5. Analyse de la concurrence (si pas de contenu dispo)
2. Proto contenu avec description breve et spécification de longueur de texte attendue
   1. On ne crée pas avec du Lorem Ipsum
   2. On crée avec du pseudo contenu
   3. Vérifier le flow et la faisabilité
   4. Quel contenu souhaite-t-on mettre en avant
      1. --> UI
3. importer le contenu existant
   1. Comment on va associer le contenu avec le design
   2. Comment on va le mettre dans la DB
   3. Comment préparer le contenu et faire les 

### Honeycumb de Morvile

Grille d'évaluation pour le contenu mais aussi le design...

Valuable:
* Useful
* Usable - utiilisable et intuitif
* Desirable - style émotif attrayant
* Findable - trouvable = référencé
* Accessible - simplicité de la rédaction et au sens handicap
* Credible - cité les sources, dates, auteurs ...

### Impact

* touche éditoriale humoristique pour créer une émotion positive
* associer le contenu au besoins - ux cards
* roue des émotion de Plutchnik

### Astuces

Réduire les textes au maximum:
* FR-BE: 50%
* NL-BE: 40%
* FR-FR: 80% (trop alambiqué)

il faut écrire en ligne comme lorsqu'on parle, très court, temps de concentration court

items clés au début

Objectif: retirer 50%

Supprimer les qui, que, dont

Utiliser les voies actives

Sujet, verbe, complément

12 mots max par phrase

La condition avant l'explication

~~téléphoner au numéro vert pour obtenir le document~~ --> *pour obtenir le document X appeler le numéro xx/xxx.xx.xx*

Le lecteur ne lit que 20% plus il perd de temps moins il consacre d'énergie au reste

Idée clé au début de l'article pas tout

Max 3 mots gras par paragraphe. Pas plus de 3 idée par paragraphe. Est-ce que les 3 mots clés suffisent à comprendre le paragraphe.

Pas d'article en gras

Les gens ne lisent pas en F, sauf sur les résultats de recherche ou les listes

En fonction du contenu: lecture en L, en E ==> tester la mise en page avec eye-tracking

**Garder une idée par ligne**

~~Un titre est plus<br>
 percutant quand il<br> va au bon endroit~~

-->

*Un titre est plus percutant<br> quand il va<br> au bon endroit*

Parler comme si on s'adressait à un enfant de 12 ans

Résumé d'intros - take away (citations, paragraphe clé, sous-titres chocs)

Crédibilité: il faut un éditeur charger de relire l'ensemble des éléments par thème

#### Outils

* Translated
* Scolarius
* Textalyser
* Yost (wordpress)

* [247 guideline](https://www.userfocus.co.uk/resources/guidelines.html) (userfocus)
* Mailchimp guidelines
* Material Design writing (google)

### Formulaire

Mot simple (pas de verbe, sujet ... veuillez ...)

Scannable directement - bouton action avec un verbe

Prénom puis Nom avec exemple via un placeholder

Commencer par le plus précis: rue, numéro ... et pas par adresse

Faire des ABtests

**Explications**: Expliquer à quoi sert chaque champs et ce qu'on va en faire, mais aussi préciser les règles du champs - champ d'aide sous forme de (i) avec les détails non nécessaire dès le début

Les trois premières chose qu'on regarde: les titres, les images et les liens

### Title

70 char -> SEO

~~Commune de schaerbeek - Accueil~~ --> Accueil - Commune de Schaerbeek

### Boutons

Rouge sang --> attire le regard du cerveau reptilien

### 404

Liens vers des pags pertinentes, notes d'humour...

### Edge cases: Empty pages / empty list

Opportunité d'onboarding

### Titres

Un titre apporte une réponse à certaines UX cards
- influence
- appartenance à un groupe
- controle
- savoir
- compétences
- autonomie
- plaisir

Utilisation des nombres:
- 50 éléments ... --> à regarder plus tard, base de données
- 10 éléments ... --> facile à lire?

En général dans les mailing et les article de masse: 10 marche le mieux

### Call to action

* Mots clés - gratuit, mon ...
* Rassurer autour du call to action
* Etre très clair sur l'action for instance: ~~Signup~~ --> Create an account


## Redaction persuasive

**B=MAT**

**B**ehavior = **M**otivation **A**ction **T**iming

Griller quelques contact pour tester les meilleurs timing pour les newsletter

Combatre l'inertie Frogg, Cugelman: Appater ou rassurer en fonction de l'audience. Eventuellement avec des parcours utilisateurs différents en fonction des comportements.

Si il va dans le detail on sait qu'il a besoin de ...

**DISC** profil - dominant - influent - conforme 

**Cialdini** arguments:
* pour que la personne se sente redevable - cadeau = redevable 
* pour rassurer
* pour s'associer à lui en lui ressemblant
* rareté

**AIDA**

**FAB** *Kevan Lee* - **F**eature **A**vantage **B**enefice
Poser des question pourquoi? pourquoi? ==> Et alors? --> Et alors?

**BAB** **B**efore - **A**fter - **B**ridge

**PAS** **P**robléme - **A**giter - **S**olution

### Outils

Le blog du rédacteur 23 mots

## SEO

Rédiger pour le SEO

* Score EAT
* keywordtool.io
* LSI - Latent Semantic Indexing (searches related)
* [Answer the public](https://answerthepublic.com)
* Google trends
* SEOQuake

# Exercice

* Challenge UX writing




