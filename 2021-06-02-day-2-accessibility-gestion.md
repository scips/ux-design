# Gestion

## Agenda

1. RoI accessibilité - motivation, arguments, budgets
2. Legislation
3. Auditer/Tester - Comment choisir un bon audit, comment le faire soi-même
4. Gérer - Comment gérer un projet incluant l'accessibilité du début à la fin
5. Conclusion

> La seule chose pire que d'être aveugle, c'est de voir mais de ne pas avoir de vision
*Hellen Keller*

## RoI Accessibilité

Comment motiver les équipes et la hiérarchie:
* Loi de Pareto attention au 15%
* Il faut donc d'autres arguments

### Outils

* Télécommande - Au départ pour les personnes qui n'avait pas ou peu de mobilité
* Econome - Inventé pour les personnes handicapées
* Email - inventé par des personnes sourdes qui voulait s'échanger des petits mots sans intermédiaires
* Eyetracking - Naviguer avec les yeux
* Head tracking - Navigation avec la tête
* Interaction avec la voix - c'est important de faire en sorte que l'intitulé, le label associé à l'icone soit simple et correspondent au mots choisi
  * 1998 Savoir comment faire scroller avec la voix, c'est compliqué, si la personne est trop lourdement handicapée, c'est compliqué
  * En mobile on fait les deux, input et output vocaux
  * 2011 Smart TV Samsung en 2011
  * Assistant vocaux
* Interaction par le regard
  * 1986 LC Technologies
  * 2011 Eye trive
  * 2015 Eye wink - intégré nativement sur apple
* Interaction via le cerveau
  * 2008 LC Technologies
  * 2011 Brain Driver
  * 2015 Muse
* Interaction par le geste

> L'accessibilité c'est la NASA du web

> Google est le plus célèbre des aveugles, sourd, muet

Google Web Vitals - Lighthouse

Lighthouse est basé sur https://www.deque.com/axe/devtools

Les check automatique représente 30% de ce qui peut être fait.

En light house on doit atteindre 100% en dessous c'est pas bon.

| | Audit |
|-|-|
| 30% | Automatique |
| 70% | Manuel |

Toutes améliorations pour les sourds, malentendants, aveugle, handicapé moteurs améliorent le SEO

### Copy writing

Niveau de langage accessible en fonction du public cible, si besoin d'aller plus loin: le falk

## Législation

Support de l'équipe légal. 

L'accessibilité est une stratégie, pas une contrainte.

* 2010 - ADA aux USA - Americans with Disabilities Act - sanctions en % de chiffres d'affaire
* 2010 - FR + UK
* 2012 - WCAG AA EU
* 2014 - WCAG AA FR
* 2016 - WCAG AA contraignant - EU
  * Etat, régions, communautés, fédéral
  * Organismes locaux
  * Organisme de droit public
* 2019 - WCAG AA contraignant obligatoire dans tous les pays de EU
  * 09/2020 - toutes les vidéos publiées doivent être accessibles
* 2025 - WCAG AA contraignant en EU pour tous les organismes

### Risques

* Risque d'image
* Organisme de contrôle
  * BOSA au niveau Fédéral - publication la liste des sites contrôlé et non accessible
    * Passe l'outil de screening
    * Présence de la déclaration d'accessibilité
    * Test de tabulation (ordre correspond au visuel)
  * Poursuite et amendes
    * Chaque citoyen peut aller en justice, les associations aussi
    * Pas de montant indicatif, ni la proportionalité
    * En France, Italie, Suède il y a une proportion

### Motivation

Priorité des demandes:
1. contact social
2. banque et assurance

![Hierarchie](images/hierarchy.png)
https://webaim.org/blog/motivating-accessibility-change/

## Auditer/Tester

### Choisir le niveau

Voir les documents annexes au WCAG 2.1

1. Utiliser les tests automatique
2. Utiliser les tests semi-automatique
3. Utiliser des simulateurs
4. Utiliser des outils réels
5. Faire des tests avec les utilisateurs réels

### Domaine d'application

* HTML
* Application
* Fichier à télécharger (doc, pdf ...)

### Niveaux

* **A**: ex: pas de règle de contraste
* **AA**:  ex: règle de contraste
* **AAA**: ex: règle de contraste adaptées

### Règles évolue

* 2018: WCAG 2.1
* 2021: WCAG 2.2
* 2025: WCAG 3.0

### WCAG

* 4 principes
  * Gestion
* 13 directives
   * Gestion
* 50 critères de succès A +AA
  * Gestion/Technique
* 100+ techniques
  * Technique

En Belgique: WCAG --> anysurfer

En France: WCAG --> RGAA (Référentiel Général D'accessibilité pour les Administration) --> AccessiWeb - très précis techniquement

Attention en cas de sous-traitance bien se baser sur les WCAG liée au pays (dans la définition du marché)

La labelisation n'est pas obligatoire

Bien demander le niveau EUROPEEN et pas les WCAGs

#### Différence entre le WCAG et les demandes européennes

* WCAG:
  * pas d'échéance
  * pas de limite de type
    * Media enregistré ET live
* En europe
  * Echéances différentes
  * Exception et dérogation
    * Vidéo: Media enregistré uniquement - pas media live
    * Vidéo: Audio description obligatoire
    * Audio: Transcription basique
    * Pas les media tiers (non financé, non développé par notre institution)

### Sélection des pages

Par visites (plus visitées)

### Tests automatisés

* ARC toolkit
* Lighthouse
* Tanaguru (capable de tester du code avant déploiement du code)
* BOSA
* Axe (capable de tester du code avant déploiement du code)
* openfed accessibility (capable de tester du code avant déploiement du code)

Utiliser ça pour faire le marché public

### Tests manuels

#### Check manuel

* Web developper toolbar
* Check exploratoire
  * Navigation clavier
    * Focus
    * Ordre
    * Tout ce qui est clickable à la souris doit être accessible (entrée et sortie) au clavier
    * utiliser TAB et SHIFT TAB
  * Zoom 200%
  * NVDA + VoiceOver et vérifier que tout passe
  * Sans mouvement (tout ce qui bouge doit s'arrêter au bout de 5 secondes sinon arretable soit même)
    * Parallaxe désactivable
  * Touch
  * Le son ne doit pas être obligatoire

#### Simulations

* JAWS
* ...

#### Tests utilisateurs

D'abord un Expert Review pour lister les scénarios qui posent des doutes et ne sélectionner que certains scénario qui fonctionne déjà parfaitement.

Ensuite avec cette sélection faire les tests.

* Excellent filtre en ergonomie
* Senior
* Personnes atteinte de handicaps

##### Pourquoi?

* Empathique
* Vous ne l'oublierez jamais
* Ils ont des techniques et des ressources assez incroyables
* Se sont des pros dans leur domaine et avec leurs outils on ne peut pas faire ça nous même

##### Attention

* Difficile de pas être directif dans les tests car on a tendance à vouloir trop scénariser

##### Etapes UX pour tester avec les personnes atteintes de handicap

* Maquettes HD non cliquable avec le VRAI texte
* Protoype HD cloquable
* Site de staging ou en prod (bcp mieux)

##### Profilage

Au minimum: 
* 1 personne aveugle - qui utilise au moins un des 3 lecteur d'écran les plus courants
* 1 personne malvoyante
* 1 senior

Aussi:
* 1 personne sourde
* 1 personne handicapée moteur

##### Recrutement

Comment les recruter:
* Contacter les associations (Phare, plein pieds, diversicom, CAP 48, ligue braille, éclat)
* Contacter sur les forums des outils (JAWS)


##### Scénarios

2-3 scénarios

ne pas faire des scénarios d'échecs forcé

30 pages clés

* navigation
* recherche

###### Préparation

* La personne est autonome
  * Formulaire de consentement à l'enregistrement doit être accessible
  * Incentive doit être accessible
  * Protocole de tests et scénario doit être accessible
  * Simplifier les termes (Simple easy question: est-ce que vous avez trouver cette tache difficile 1-5 ==> -2 à +2)
  * Canal de feedback - Traducteur en langue des signes dispo par exemple
  * Préparer les déplacement (c'est vous qui bougez parce qu'il a tout son matériel chez lui)
    * En plus ça fait du ghosting

###### Psychologie de l'utilisateur

Attention à l'échec

Bien expliqué que ce qu'on teste ce sont des choses compliquée, il faut les mettre à l'aise.

Il ne doivent pas culpabiliser

Surtout ne pas donner les réponses

Surtout ne pas féliciter ni valoriser c'est encore plus humiliant.

Insister sur la valeur du feedback.

Ne pas vous identifier comme l'auteur, au contraire, dites que vous êtes une société externe d'audit.

Le problème vient du site pas des gens.

##### Compétence de l'UX researcher pour accompagner

Les personnes aveugle vont super vite et skip souvent les élements.

Vous devez connaitre à fond le scénario

##### Analyse

Ne pas mélanger UX et accessiblité:
* Analyser l'accessibilité à part, pas via des personas et ne pas les lier aux personas

A quoi ressemble un bon rapport? 4 qualités principales
* Une liste claire et prioritisée (Legal + BV + UV/SP) des problématiques rencontrées
* Du verbatim des testeurs
* Une liste des pistes de solutions
* Une estimation du temps nécessaire pour faire les modifications

* Condition du test + profil du testeur (protocole)
* Liste des taches à réalisée avec réussi ou raté et exemple lorsque c'est raté
* Une note de sévérité
* Une recommandation pour corriger avec un lien vers une référence

* **périmètre**: Quel est le périmètre des tests et quels partie n'est pas encore couverte
* **complexité**: Adapter le rapport à l'audience
* **remédiation**: Comment re-tester au mieux

**BV** = EV + UV (Enterprise Value + User Value)

### Rapport d'audit

## Gérer le porjet

* A penser dès le départ sinon ça coute vraiment cher

### Prestataire

Dans le marché d'audit

* **Certification WAS c'est le top**
* Accessiweb
* Koena
* Anysurfer

Dans le marché de projet/produit

* Rajouter des règles lors des appels d'offre: validtié W3C avec 0 erreurs
* BOSA 100%



## Ressources

* atalan.fr
* Headings map plugin
* Google chrome ligthhouse
* Google chrome webvitals
* Bosa tools
* AccessiWeb RGAA
* WCAG quick ref dans google
  * A donner aux designer, techniciens ...
  * A suivre pour la priorité du travail
* https://www.deque.com/axe/devtools



## Todo

* XBOX Voice command exemples
* Samsung HTML5 Voice command
* Mode paysage/vertical obligatoire pour les apps
* Google Web Vitals - Lighthouse
* Ne pas mettre des intitulés de navigation en H1-H6
* Suivre l'ordre des headings
* Si il y a des éléments répétitifs on peut sauter les headings (composant réutilisé)
* Si il y a des cadres on peut avoir des titres à nouveau
* Aria id doivent être uniques
* Focus customisé et navigation kiosque
* Synthèse powerpoint à présenter au comité éditorial
* Organiser les tests utilisateurs
  * Organiser le recrutement 
* Identifier les règles du CSA en terme d'accès aux personnes en situation de handicap et estimer la différence avec les règles WCAG