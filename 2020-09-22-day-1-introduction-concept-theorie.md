# Introduction Concepts et Théorie, ROI

*by Alexandre Dehon*

**slido: #A474**

* UX = 25% des recrutements

UX à l'unif différent car ancré dans un contexte scientifique

## Formateur: Alexandre Dehon - ULB

* Laboratoire d'usability --> Nicolas dehue
* Master Ergo & Sociologie
* Anais Digital
* **Flupa** (assoc. Francophone à propos de l'expérience):
    * World usability day (15 novembre) orga d'un event ULB - Flupa - Anais Digital - AI - RTBF

## Formation

### Personnes de références

* Anaïs: Logistique - attestations
* Alexandre: Pedagogie - Travail de fin de formation

### Outils

#### Teams

* Plannifiier les sessions de coaching
* Calendrier
* Discussion entres étudiants

#### Mural

* Modèle mental

#### Sli.do

* Interaction pendant les cours

### Evaluation

* **Quizz**: 40 questions tous modules confondu ~2h en Décembre
* **Travail individuel**: 1 powerpoint - 20 slides - métant en application 1 méthodologie qui implique l'utilisateur - idéalement sur un projet professionnnel. Slide Auto-explicite - pas de présentation - évaluation par Bruno et Alexandre. Avec 3x20 minutes de coaching via Teams.

## Introduction

### Qu'est-ce que c'est l'UX?

- C'est offrir une expérience d'interaction avec le monde à un ensemble d'individus pour atteindre un objectif
- Concevoir un produit au service des utilisateurs
- Processus dont le but est de joindre l'utile à l'agréable

Offrir une expérience d'interaction dans le cadre de la conception d'un produit au service des utilisateurs au sein d'un processus dont le but est de joindre l'utile à l'agréable.

[Fig 1. - Mots qui synthétise l'ux]

#### Le problème du toboggan

Nous concepteur / desginer on imagine un produit avec une série de caractéristiques et l'utilisateur en fait complètement autre chose.

Vauban avait déjà ce problème, il fesait des superbe machine pour déplacer des charges lourdes et les gens les utilisaient n'importe comment. Idem pour Da vinci.

Pendant la seconde guerre mondiale - conception d'avion avec de plus en plus d'indicateur compliqué dans les cockpits.

Plus ils développent les cockpits plus les pilotes se crashent ...

L'être humain est limité, il va falloir adapter la conception à l'être humain et non l'inverse.

C'est un problème également rencontré dans les usines. Les gens se blessent, se fatiguent ...

Comment résoudre ce problème? 

#### Comment résoudre

1. S'interresser à l'être humain
2. Critère de bonne pratique - basé sur la qualité normative
3. Le voir dans la globalité - Systemic models

L'expérience utilisateur est un système, on est ici dans la psychologie.

#### S'interresset à l'être humain

**Patricia Moore** designer industrielle doit innover dans le segment des personnes agées. Pendant 2 ans elle s'habille et vit comme une vieille dame. Elle se contraignait à agir comme une personne agée pour rentrer dans les chaussure (dans la peau) des personnes agées.

* Elle a reconçu les portes de frigo - toute simple
* Elle a recréer tous les ustenciles avec du grip (anti-dérapant)

 > Première étape: rentrer dans la peau des utilisateurs - empathie

#### UX = Discipline

* Adapter à l'homme
* Optimiser le système
* Etude scientifique

Différente discipline qui s'entremelle - l'expérience utilisateur est multi-disciplinaire. (Psychologie, sociologie, physiologie, ...)

[Fig. 2]

UX = hUman eXperience

1. Sciences de l'humain
2. Vision Technocentré --> Vision Anthopocentré
3. Relation humain <-> artefact 

Pas besoin de voir 10 vidéos sur internet pour comprendre

#### UX = système normatif

On va appliquer des critères - des bonnes pratiques

[Fig. 3]

ISO 9241, ISO 25000 - modèle de conception centrée sur l'humain

* **Utile** dans le **contexte** est-ce que cet objet à un intéret pour accomplir ma tache
* **Utilisable** fonctionnel hors du **contexte**

* **Efficience** nombre d'erreur, difficulté, temps pour accomplir sa tache
* **Acceptabilité sociale** Accepter d'utiliser le système
* **Acceptabilité pratique** Utilité
* **Integration dans l'environnement**
* **Fiabilité**
* **Coût**

*Modèle d'acceptation de Nielsen*

Tout ceci reste du point de vue du concepteur

#### Point de vue global - approche systémique

* Qualité pragmatiques
    * Do Goals
* Qualité Hedoniques
    * Be goals (epanouissement, aspect original)

[Fig. 4]

*Modèle de Mahike et Thüring*

##### Exemple - Perrier
1. Historique
2. Cocktail pamplemouse

**Mesure**
* du rythme cardiaque: très fort
* de l'achevement de la tache: personne

**Qualités**
* Hédonique: forte (évoque bcp)
* Pragmatique: faible (complexe, inutile)


##### Exemple - PDF to Doc
1. Transformer un pdf en document word

**Qualités**
* Pragamatique: forte - simple
* Hédonique: fabile - pub, sécurité, pas de marque de confiance

#### UX cards - Sheldon théorie psychologie


* https://uxmind.eu/portfolio/ux-design-and-evaluation-cards/ (from https://uxmind.eu/portfolio/ux-design-and-evaluation-cards/)



8 besoins psychologique de [Sheldon](https://link.springer.com/article/10.1007/s11031-019-09818-1) (suppression de la luxure pour le UX)

* https://carinelallemand.files.wordpress.com/2015/12/ux-cards_lallemand_fr_v1.pdf
* [Sheldon's basic needs pdf resume study](Vansteenkiste2020_Article_BasicPsychologicalNeedTheoryAd.pdf)

On se réfère au minimum aux besoins de Sheldon.

#### Questionnaire UEQ

Existe en [long](https://ueqtryitout.ueq-research.org/full_ueq_questionnaire.html) et en [court](https://ueqtryitout.ueq-research.org/short_ueq_questionnaire.html) - [Source ueq-online.org](https://www.ueq-online.org/)

En demandant aux utilisateur de compléter le questionnaire on va pouvoir déterminer les qualités hédoniques et pragmatiques.

Ex. What's app

* Facilitant ==> Pragmatique
* Simple ==> Pragmatique
* Efficace ==> Pragmatique
* Clair ==> Pragmatique
* Ennuieuse ==> Hédonique
* Interet ==> Hédonique? Lié au contenu
* Conventionnelle ou originale ==> Hédonique

#### 4 types de produits

### Expérience Globale 

Donald A. Norman --> **UX**


[Fig. 5]

Impact psychologique de l'expérience utilisateur est importante et temporelle.

#### Temporalité

* Avant - appréhensions ...
* Au moment même
* Juste après
* Dans le temps - se répète, se modifie, s'érode ou s'améliore

[Fig. 6]

**Pics** et **gouffres**

* **Pics** vers le haut (expérience)
* **Gouffres** vers le bas (expérience)

##### Gérer la temporalité

Il est aussi interressant de décrire la temporalité dans un sous-cas bien précis d'un produit: exemple attente d'un Uber.

- transparence opérationnelle - explique tout le processus et permet de gérer la temporalité
- gradient de l'objectif - temps restant - progression

### ROI

Impact de prendre l'expérience utilisateur en compte sur les chiffres de ventes, le taux de conversion ...

## Conclusion

1. UX = ensemble de modèles
2. Expérience avec souvenir positif qui permet la mémorisation dont le produit est un medium seulement => Globlité

## Exemples
* https://asphalte.com/ - tone of voice, personna bien défini, customer journey complete prise en compte

# Travail de Fin de Formation

## Objectifs

Appliquer une méthodologie vue lors de la formation dans un cas pratique. 

Expliquer et justifier sa démarche suivant les principes vus lors des différentes leçons.

Présenter les résultats de la démarche de manière claire et argumentée

## Choix du cas pratique

Projet réel ou à construire (pas de projet “fictif”)

Pas de limite de secteur ou d’industrie

## Contenu attendu

Définition du problème

Motivation de l’intérêt du cas pratique choisi

Scope de l’intervention

Description des utilisateurs cibles

Choix argumenté d’une méthodologie suivant les principes vus lors des différents cours, description de l’application de cette méthodologie

Présentation des résultats

Autocritique du projet (méthodologie choisie, résultats…)

## Livrable

Présentation écrite, type powerpoint, de 20 slides

Un équilibre entre visuels et explications sera apprécié.

La clarté et la  structuration du rapport seront évaluées.

Date maximum de remise du travail: le 11 décembre 2020

A envoyer à alexandre.dehon@anais.digital 

Le travail ne sera pas présenté devant un jury

## Critères d’évaluation

Respect des consignes

Présentation générale (clarté, lisibilité, mise en page… )

Définition du problème

Choix méthodologique (motivation du choix, pertinence…)

Application de la méthodologie choisie

Analyse et communication  des résultats

Compréhension des concepts

Rétrospective, auto-critique

